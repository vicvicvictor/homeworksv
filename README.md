<h1 style="color: #900C3F">Homeworks - Programming 2</h1>

<h2 style="color: #E37383">Student : Victor Leon Villca Silva</h2>
![Logo](https://i.postimg.cc/k4Q1wsk9/img-girl-and-window.png)
## What is this repository about?

This repository is for all my homeworks from this day on. In this repository, I will have 
each homework of the semester.
-----
<h1 style="color:#F88379;">Homework_Tuesday08_08 Monday 25-07</h1> 

## Code we had in class about this homework


```
public class People{
   public int age;
   public String name;
   public String lastName;
   String GREET="hello";
   
   public String greet(){
     return GREET+" my name is "+name;
   }
 }
```
##What i do in this homework:
Encapsulate properly the class by providing read accessors

-----
<h1 style="color:#F88379;">Homework_Tuesday08_08 Monday 25-07 (2)</h1>

## Important code for the homework



```
@Override
    public int numberOfDistinctColors(List<Color> mysteryColors) {
        ArrayList<String> colors = new ArrayList<>();
        for (Color color:
             mysteryColors) {
            if (! isStringInList( colors, color.name()) ){
                colors.add(color.name());
            }
        }
        return colors.size();
    }

    @Override
    public int colorOccurrence(List<Color> mysteryColors, Color color) {
        int counter = 0;
        for (Color c:
             mysteryColors) {
            if (c.name().equals(color.name())){
                counter++;
            }
        }
        return counter;
    }
```
##What i do in this homework:
In this homework, I am asked to do two tasks.

- The first one is to create a method that determines how many distinct colors are in the given list.
- The second one is to create a method that counts how often a specific color is in the given list.
- -----
<h1 style="color:#F88379;">Homework_Tuesday08_08 Tuesday 26-07</h1>

## Important code for the homework
This are the most important functions of the program:


```
class Canvas {
  public Canvas(int width, int height) {
  }
  
  public Canvas draw(int x1, int y1, int x2, int y2) {
    return this;
  }
  
  public Canvas fill(int x, int y, char ch) {
    return this;
  }
  
  public String drawCanvas() {
    return "draw the canvas with borders";
  }
}
```
##What i do in this homework:
I wrote a simple console-style drawing board program.

###Functions of the drawing board program.
The drawing board is very simple so only these functions are supported.


###Create canvas	
Create new empty canvas (filled with ' ') with given width and height
###Draw line / rectangle	
Draw the line with 'x' which connects the given points.
If the points are diagonal, a rectangle instead of diagonal line should be drawn
###Fill color	
Fill the entire area connected to the given point with given character, also known as "bucket fill" in paint programs
- -----
<h1 style="color:#F88379;">Homework_Tuesday08_08 Wednesday 27-07</h1>

## Example:


```
input array: [3, -1, -1, -1, 2, 3, -1, 3, -1, 2, 4, 9, 3]
ouptut: 5 
```
##What i do in this homework:
Complete the function to find the count of the most frequent item of an array. You can assume that input is an array of integers. For an empty array return 0



- -----
 
<h1 style="color:#F88379;">Homework_Tuesday08_08 Thursday 28-07</h1>

## Important code for the homework
The process should be like this:


```
[2, 1, 2, 2, 6, 5, 0, 2, 0, 5, 5, 7, 7, 4, 3, 3, 9]  -->
         2+2+6       0+2+0     5+5+7+7       3+3+9
[2, 1,   10,    5,    2,        24,     4,   15   ] -->
                               2+24+4
[2, 1,   10,    5,             30,           15   ]
The length of final array is 6
```
##What i do in this homework:
Given an array of integers, sum consecutive even numbers and consecutive odd numbers. Repeat the process while it can be done and return the length of the final array.

---

<h1 style="color:#F88379;">Homework_Tuesday08_08 Friday 29-07</h1>

##What is this homework about?
In this homework I have to create a method named "rotate" that returns a given array with the elements inside the array rotated n spaces.
###Requirements:
If n is greater than 0 it should rotate the array to the right. If n is less than 0 it should rotate the array to the left. If n is 0, then it should return the array unchanged.
##Example:

```
with array [1, 2, 3, 4, 5]

n = 7        =>    [4, 5, 1, 2, 3]
n = 11       =>    [5, 1, 2, 3, 4]
n = 12478    =>    [3, 4, 5, 1, 2]
```

----


<h1 style="color:#F88379;">Game Monday 01-08</h1>

##What is this homework about?
Implementar un juego que use un mapa (10x10) de coordenadas X Y, donde un jugador será ubicado (en una casilla) en una posición aleatoria dentro el mapa, luego 5 obstáculos dentro el mapa en posiciones aleatorias.
###Requirements:
el jugador debe intentar llegar a la esquina inferior derecha del mapa, un paso a la vez, pasando de su posición actual a la derecha, izquierda, arriba, abajo. Siempre y cuando no se encuentre con un obstáculo.
##Example of my game:

```
WELCOMEEEE TO THE GAME <THE MAP>
Please, enter your name: 
Victor
Hello player Victor, this are the game instruccions:
1. Your goal is to get to the bottom right corner without touching any obstacles
2. You can move with numbers.
3. Press 1 to go up
4. Press 2 to go down
5. Press 3 to go to the right
6. Press 4 to go to the left
GOOD LUCK, Victor.

· · · · · · · 0 0 · 
· · · · 0 · 0 · · · 
· · · · · · · · · · 
· · · X · · · · · · 
· · · · · · · · · · 
· · · · · · · · · · 
· · · · · · · · · · 
· 0 · · · · · · · · 
· · · · · · · · · · 
· · · · · · · · · · 
1


· · · · · · · 0 0 · 
· · · · 0 · 0 · · · 
· · · · · · · · · · 
· · · · · · · · · · 
· · · · · · · · · · 
· · · · · · · · · · 
· · · · · · · · · · 
· 0 · · · · · · · · 
· · · · · · · · · · 
· · · · · · · · · X 
CONGRATULATIONS, you won!!


```

----
<h1 style="color:#F88379;">Homework_Tuesday08_08 Tuesday 02-08</h1>

##What is this homework about?
Crear una clase JUArrayList (basado en arreglos) que implemente la interface: “java.util.List”
###Requirements:
el jugador debe intentar llegar a la esquina inferior derecha del mapa, un paso a la vez, pasando de su posición actual a la derecha, izquierda, arriba, abajo. Siempre y cuando no se encuentre con un obstáculo.
##Methods to implement
###Required:
-int size()

-boolean isEmpty()

-Iterator<Integer> iterator()

-boolean add(Integer integer)

-boolean remove(Object o)

-void clear()

-Integer get(int index)

-Integer remove(int index)
###Opcional(for more points):
-contains(Object o)

-Integer set(int index, Integer element)

-void add(int index, Integer element)

-Object[] toArray()

-<T> T[] toArray(T[] a)
##Example of UML-DIAGRAM of the homework:

![Logo](https://i.postimg.cc/k5nbRmTP/JUArray-List-UML.png)

----

<h1 style="color:#F88379;">Homework_Tuesday08_08 Friday 05-08</h1>

##What is this homework about?
In this homework I have to create to methods:
 - getNth();
 - append();

For Nodes in LinkedLists
###Requirements:
####getNth()
It  takes a linked list and an integer index and returns the node stored at the Nth index position. GetNth() uses the C numbering convention that the first node is index 0, the second is index 1, ... and so on.
####append()
It appends one linked list to another. The head of the resulting list should be returned.
##Example:
Append()
```
var listA = 1 -> 2 -> 3 -> null
var listB = 4 -> 5 -> 6 -> null
append(listA, listB) === 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> null
```
GetNth()

```
getNth(1 -> 2 -> 3 -> null, 0).data === 1
getNth(1 -> 2 -> 3 -> null, 1).data === 2
```
----
<h1 style="color:#F88379;">Homework_Tuesday08_08 Tuesday 08-08</h1>

##What is this homework about?
Crear una clase JULinkedList (basado en Nodos), aplicando correctamente los principios de POO, que implemente la interface: “java.util.List” (List<T>).
###Requirements:
Implementar diferentes pruebas para 2 tipos de listas: String, Integer.
Incluir el Diagrama UML.
##Methods to implement
###Required:


- int size()
- boolean isEmpty()
- Iterator<T > iterator()
- boolean add(T element)
- boolean remove(Object o)
- void clear()
- T get(int index)
- T remove(int index)
- contains(Object o)
- T set(int index, T element)
- int indexOf(Object o)
- int lastIndexOf(Object o)


###Opcional(for more points):
- void add(int index, T element)
- Object[] toArray()
- <T> T[] toArray(T[] a)
- List<T> subList(int fromIndex, int toIndex)
##Example of UML-DIAGRAM of the homework:

![Logo](https://i.postimg.cc/W3Kxh8xv/Homework-Tuesday08-08-UML.png)

----
----
<h1 style="color:#F88379;">Homework_Friday 12-08</h1>

##What is this homework about?
Implementar los métodos `selectionSort` y `bubbleSort` de la interfaz `Bag` que se revisó en la clase de hoy:
###Requirements:
Implementado los correspondientes algoritmos de ordenamiento para la estructura de datos definida en la clase `LinkedBag` (pueden implementar otros métodos de ordenamiento diferentes al selection y bubble por algún punto extra siempre y cuando los describan bien y tengan sus propias pruebas unitarias). Mandar un MR, con la solución, UML, y sus pruebas unitarias.
No cambiar las implementaciones ni definiciones de `Bag`, `Node` `LinkedBag`, Solo en los métodos indicados (`selectionSort` y `bubbleSort`) de la clase `LinkedBag` si agregan algún método en esta clase debe ser `private`, salvo la inclusión de nuevos métodos de ordenamiento alternativos, ejemplo `mergeSort` que se agregaría ese comportamiento en la interfaz `Bag` y se lo implantaría en la clase ` LinkedBag `.
##Methods to implement

- void selectionSort()
- boolean bubbleSort()
  
- ![Logo](https://i.postimg.cc/3J6gWnSj/Homework-Friday-12-08.png)
----
<h1 style="color:#F88379;">Homework_Monday 15-08</h1>

##What is this homework about?
mplementando la interface:

public interface List<<T extends Comparable<T>> {

Opción F: clase ` DoublyCircularLinkedList` que implemente los métodos.

    int size()

    isEmpty()

    add(T data)

    T get(int index)

    boolean remove(T data)


}
###Requirements:
En todos los casos su clase Nodo debe se externo a la lista y su campo `T data` privado sin `setData`.

Para las opciones: A, B, D, E, G, H, las pruebas unitarias deben enfocarse en sus métodos `sort`, y deben contemplar varios casos.
Para las opciones C y F, sus pruebas unitarias deben enfocarse en  los métodos add y remove, con varios casos, y también un par de pruebas exclusivas cambiando ambos métodos, agregando ítems al principio y al final; removiendo ítems del principio y del final,  y de en medio, combinado o alternando operaciones, para 2 diferentes escenarios.

Todas las pruebas unitarias con ítems, Integer y otra con String.



De los que necesiten, se pueden basar en los siguientes links:

https://en.wikipedia.org/wiki/Merge_sort
https://en.wikipedia.org/wiki/Doubly_linked_list

Aclaración: en la clase ` DoublyLinkedList `, el `next` del último elemento es null, y ` previous` de `head` también es null. En el caso de DoublyCircularLinkedList, DoublyCircularLinkedList, el `next` del último elemento es apunta al primer elemento (al mismo que `head`), y ` previous` de `head` apunta al último elemento.
##Example of UML-DIAGRAM of the homework:

![Logo](https://i.postimg.cc/cH4KFnk4/Homework-Monday18-08-UML.png)

----
----
<h1 style="color:#F88379;">Homework_Thursday 18-08</h1>

##What is this homework about?
Sobre la tarea previa, y mantenido las opciones que les toco a cada uno, implementar los siguientes métodos (en su clase Lista) en cada opción:

Opción C, F:
void xchange(int x, int y); // intercambia un elemento en el índice x con otro en el índice y

##Methods to implement

- void selectionSort()
- boolean xchange()

----
