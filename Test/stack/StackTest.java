package stack;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StackTest {
    Stack<Integer> integerStack = new Stack<>();
    Stack<String> stringStack = new Stack<>();
    @Test
    void push() {
        /*
        INTEGER PART
         */

        assertTrue(integerStack.isEmpty());
        integerStack.push(1);
        assertFalse(integerStack.isEmpty());
        integerStack.push(2);
        integerStack.push(3);
        assertEquals(3, integerStack.size());
        integerStack.push(4);
        integerStack.push(5);
        integerStack.push(6);
        assertEquals(6, integerStack.size());

        assertEquals("{Stack Top-> 6  {data-> 1, next-> {data-> 2, next-> {data-> 3, next-> {data-> 4, next-> {data-> 5, next-> {data-> 6, next-> null}}}}}}}", integerStack.toString());



        /*
        STRING PART
         */

        assertTrue(stringStack.isEmpty());
        stringStack.push("10");
        assertFalse(stringStack.isEmpty());
        stringStack.push("20");
        stringStack.push("30");
        assertEquals(3, stringStack.size());
        stringStack.push("40");
        stringStack.push("50");
        stringStack.push("60");
        assertEquals(6, stringStack.size());

        assertEquals("{Stack Top-> 60  {data-> 10, next-> {data-> 20, next-> {data-> 30, next-> {data-> 40, next-> {data-> 50, next-> {data-> 60, next-> null}}}}}}}", stringStack.toString());
    }

    @Test
    void pop() {
          /*
        INTEGER PART
         */

        assertTrue(integerStack.isEmpty());
        integerStack.push(1);
        integerStack.push(2);
        integerStack.push(3);
        integerStack.push(4);
        integerStack.push(5);
        integerStack.push(6);
        assertEquals(6, integerStack.size());
        assertEquals("{Stack Top-> 6  {data-> 1, next-> {data-> 2, next-> {data-> 3, next-> {data-> 4, next-> {data-> 5, next-> {data-> 6, next-> null}}}}}}}", integerStack.toString());

        assertEquals(6, integerStack.pop());
        assertEquals(5, integerStack.size());
        assertEquals(5, integerStack.pop());
        assertEquals(4, integerStack.pop());
        assertEquals(3, integerStack.size());
        assertEquals(3, integerStack.pop());
        assertEquals(2, integerStack.pop());
        assertEquals(1, integerStack.pop());
        assertEquals(null, integerStack.pop());
        assertEquals(0, integerStack.size());

        integerStack.push(1);
        assertEquals("{Stack Top-> 1  {data-> 1, next-> null}}", integerStack.toString());




        /*
        STRING PART
         */

        assertTrue(stringStack.isEmpty());
        stringStack.push("10");
        stringStack.push("20");
        stringStack.push("30");
        stringStack.push("40");
        stringStack.push("50");
        stringStack.push("60");
        assertEquals(6, stringStack.size());
        assertEquals("{Stack Top-> 60  {data-> 10, next-> {data-> 20, next-> {data-> 30, next-> {data-> 40, next-> {data-> 50, next-> {data-> 60, next-> null}}}}}}}", stringStack.toString());

        assertEquals("60", stringStack.pop());
        assertEquals(5, stringStack.size());
        assertEquals("50", stringStack.pop());
        assertEquals("40", stringStack.pop());
        assertEquals(3, stringStack.size());
        assertEquals("30", stringStack.pop());
        assertEquals("20", stringStack.pop());
        assertEquals("10", stringStack.pop());
        assertEquals(null, stringStack.pop());
        assertEquals(0, stringStack.size());

        stringStack.push("10");
        assertEquals("{Stack Top-> 10  {data-> 10, next-> null}}", stringStack.toString());
    }

    @Test
    void peek() {
        //solo los catolicos creen el Espiritu Santo
        /*
        INTEGER PART
         */
        integerStack.push(1);
        assertEquals(1,integerStack.peek());
        integerStack.push(2);
        assertEquals(2,integerStack.peek());
        integerStack.push(3);
        assertEquals(3,integerStack.peek());
        integerStack.push(4);
        assertEquals(4,integerStack.peek());
        integerStack.push(5);
        assertEquals(5,integerStack.peek());
        integerStack.push(6);
        assertEquals(6,integerStack.peek());
        integerStack.push(7);
        assertEquals(7,integerStack.peek());
        integerStack.push(8);
        assertEquals(8,integerStack.peek());
        integerStack.push(9);
        assertEquals(9,integerStack.peek());
        integerStack.push(10);
        assertEquals(10,integerStack.peek());
        integerStack.pop();
        assertEquals(9,integerStack.peek());
        integerStack.pop();
        assertEquals(8,integerStack.peek());
        integerStack.pop();
        assertEquals(7,integerStack.peek());
        integerStack.pop();
        assertEquals(6,integerStack.peek());
        integerStack.pop();
        assertEquals(5,integerStack.peek());
        integerStack.pop();
        assertEquals(4,integerStack.peek());
        integerStack.pop();
        assertEquals(3,integerStack.peek());


        /*
        STRING PART
         */
        stringStack.push("10");
        assertEquals("10",stringStack.peek());
        stringStack.push("20");
        assertEquals("20",stringStack.peek());
        stringStack.push("30");
        assertEquals("30",stringStack.peek());
        stringStack.push("40");
        assertEquals("40",stringStack.peek());
        stringStack.push("50");
        assertEquals("50",stringStack.peek());
        stringStack.push("60");
        assertEquals("60",stringStack.peek());
        stringStack.push("70");
        assertEquals("70",stringStack.peek());
        stringStack.push("80");
        assertEquals("80",stringStack.peek());
        stringStack.push("90");
        assertEquals("90",stringStack.peek());
        stringStack.push("100");
        assertEquals("100",stringStack.peek());
        stringStack.pop();
        assertEquals("90",stringStack.peek());
        stringStack.pop();
        assertEquals("80",stringStack.peek());
        stringStack.pop();
        assertEquals("70",stringStack.peek());
        stringStack.pop();
        assertEquals("60",stringStack.peek());
        stringStack.pop();
        assertEquals("50",stringStack.peek());
        stringStack.pop();
        assertEquals("40",stringStack.peek());
        stringStack.pop();
        assertEquals("30",stringStack.peek());

    }
}