import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BoardTest {

    @Test
    void getBoardOnString() {
        Board board = new Board(10);
        board.fillBoard();
        Assertions.assertEquals("··········\n" +
                "··········\n" +
                "··········\n" +
                "··········\n" +
                "··········\n" +
                "··········\n" +
                "··········\n" +
                "··········\n" +
                "··········\n" +
                "··········\n", board.getBoardOnString());
    }

    @Test
    void getSizeTotal() {
        Board board = new Board(10);
        board.fillBoard();
        Assertions.assertEquals(100,board.getBoardTotalSize());
    }

    @Test
    void getItem() {
        Board board = new Board(10);
        board.fillBoard();
        Assertions.assertEquals("·", board.getItem(5,3));
    }

    @Test
    void verifyIfPostionLegal() {
        Board board= new Board(10);
        board.fillBoard();
        // To move Up
        Assertions.assertEquals(false, board.verifyIfPostionLegal(0,5,1));
        // To move Down
        Assertions.assertEquals(false, board.verifyIfPostionLegal(9,1,2));
        // To move to the left
        Assertions.assertEquals(false, board.verifyIfPostionLegal(0,0,4));
        // To move to the right
        Assertions.assertEquals(false, board.verifyIfPostionLegal(0,9,3));
    }
}