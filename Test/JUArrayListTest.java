import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;

class JUArrayListTest {
    JUArrayList juArrayList = new JUArrayList();
    @Test
    void size() {
        Assertions.assertEquals(0, juArrayList.size());
        juArrayList.add(1);
        juArrayList.add(1);
        juArrayList.add(1);
        juArrayList.add(1);
        juArrayList.add(1);
        Assertions.assertEquals(5, juArrayList.size());
        juArrayList.add(1);
        juArrayList.add(1);
        juArrayList.add(1);
        juArrayList.add(1);
        Assertions.assertEquals(9, juArrayList.size());
        juArrayList.add(1);
        juArrayList.add(1);
        juArrayList.add(1);
        Assertions.assertEquals(12, juArrayList.size());
        juArrayList.add(1);
        juArrayList.add(1);
        Assertions.assertEquals(14, juArrayList.size());
    }

    @Test
    void isEmpty() {

        assertTrue(juArrayList.isEmpty());
        juArrayList.add(1);
        juArrayList.add(2);
        juArrayList.add(3);
        assertFalse(juArrayList.isEmpty());

    }

    @Test
    void iterator() {
        //I set my array
        juArrayList.setArray(new Integer[]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20});
        //I create my iterator
        Iterator<Integer> iterator= juArrayList.iterator();
        //I crate my expected array with all the changes that the iterator should do
        Integer[] expectedArray = new Integer[]{1,0,3,0,5,0,7,0,9,0,11,0,13,0,15,0,17,0,19,0};
        //I create an array with the same size as juArrayList one
        Integer[] actualArray = new Integer[juArrayList.size()];
        int index =0;
        //I iterate and add the next value to my actual ARRAY
        while ( iterator.hasNext() ) {
            int n = iterator.next();
            actualArray[index]=n;
            index++;
        }
        Assertions.assertArrayEquals(expectedArray,actualArray);
    }

    @Test
    void add() {
        Assertions.assertArrayEquals(new Integer[]{},juArrayList.getArray());
        juArrayList.add(1);
        juArrayList.add(2);
        juArrayList.add(3);
        juArrayList.add(4);
        Assertions.assertArrayEquals(new Integer[]{1,2,3,4},juArrayList.getArray());
        juArrayList.add(5);
        juArrayList.add(6);
        Assertions.assertArrayEquals(new Integer[]{1,2,3,4,5,6},juArrayList.getArray());
        juArrayList.add(7);
        juArrayList.add(8);
        juArrayList.add(9);
        juArrayList.add(10);
        juArrayList.add(11);
        juArrayList.add(12);
        Assertions.assertArrayEquals(new Integer[]{1,2,3,4,5,6,7,8,9,10,11,12},juArrayList.getArray());
    }

    @Test
    void removeIndex() {
        juArrayList.add(1);
        juArrayList.add(2);
        juArrayList.add(3);
        juArrayList.add(4);
        Assertions.assertArrayEquals(new Integer[]{1,2,3,4},juArrayList.getArray());
        juArrayList.remove(0);
        Assertions.assertArrayEquals(new Integer[]{2,3,4},juArrayList.getArray());
        juArrayList.remove(2);
        Assertions.assertArrayEquals(new Integer[]{2,3},juArrayList.getArray());
        juArrayList.remove(1);
        Assertions.assertArrayEquals(new Integer[]{2},juArrayList.getArray());
    }

    @Test
    void clear() {
        juArrayList.add(1);
        juArrayList.add(2);
        juArrayList.add(3);
        assertFalse(juArrayList.isEmpty());
        Assertions.assertEquals(3, juArrayList.size());
        juArrayList.clear();
        Assertions.assertEquals(0, juArrayList.size());
        assertTrue(juArrayList.isEmpty());
    }


    @Test
    void get() {
        juArrayList.add(1);
        juArrayList.add(2);
        juArrayList.add(3);
        juArrayList.add(4);
        juArrayList.add(5);
        juArrayList.add(6);
        Assertions.assertEquals(1,juArrayList.get(0));
        Assertions.assertEquals(2,juArrayList.get(1));
        Assertions.assertEquals(3,juArrayList.get(2));
        Assertions.assertEquals(4,juArrayList.get(3));
        Assertions.assertEquals(5,juArrayList.get(4));
    }

    @Test
    void removeObject() {
        Integer n1 = 1;
        Integer n2 = 2;
        Integer n3 = 3;
        Integer n4 = 4;
        Integer n5 = 5;
        juArrayList.add(n1);
        juArrayList.add(n2);
        juArrayList.add(n3);
        juArrayList.add(n4);
        juArrayList.add(n5);
        Assertions.assertArrayEquals(new Integer[]{1,2,3,4,5},juArrayList.getArray());
        juArrayList.remove(n5);
        Assertions.assertArrayEquals(new Integer[]{1,2,3,4},juArrayList.getArray());
        juArrayList.remove(n1);
        Assertions.assertArrayEquals(new Integer[]{2,3,4},juArrayList.getArray());
        juArrayList.remove(n3);
        Assertions.assertArrayEquals(new Integer[]{2,4},juArrayList.getArray());
    }

    @Test
    void set() {
        juArrayList.add(1);
        juArrayList.add(2);
        juArrayList.add(3);
        juArrayList.add(4);
        Assertions.assertArrayEquals(new Integer[]{1,2,3,4},juArrayList.getArray());
        juArrayList.set(0,100);
        Assertions.assertArrayEquals(new Integer[]{100,2,3,4},juArrayList.getArray());
        juArrayList.set(3,23);
        Assertions.assertArrayEquals(new Integer[]{100,2,3,23},juArrayList.getArray());
        juArrayList.set(1,45);
        Assertions.assertArrayEquals(new Integer[]{100,45,3,23},juArrayList.getArray());

    }

    @Test
    void testAdd() {
        juArrayList.setArray(new Integer[]{1,2,3,4,5});
        Assertions.assertArrayEquals(new Integer[]{1,2,3,4,5},juArrayList.getArray());
        juArrayList.add(0,100);
        Assertions.assertArrayEquals(new Integer[]{100,1,2,3,4,5},juArrayList.getArray());
        juArrayList.add(3,23);
        Assertions.assertArrayEquals(new Integer[]{100,1,2,23,3,4,5},juArrayList.getArray());
        juArrayList.add(1,45);
        Assertions.assertArrayEquals(new Integer[]{100,45,1,2,23,3,4,5},juArrayList.getArray());
    }

    @Test
    void contains() {
        juArrayList.setArray(new Integer[]{1,2,3,4,5,45,23,100});
        assertTrue(juArrayList.contains(1));
        assertFalse(juArrayList.contains(44));
        assertTrue(juArrayList.contains(100));
        assertFalse(juArrayList.contains(24));
        assertTrue(juArrayList.contains(45));
        assertFalse(juArrayList.contains(230));

    }

    @Test
    void toArray() {
        juArrayList.setArray(new Integer[]{1,2,3,4,5,6});
        Assertions.assertArrayEquals(new Object[]{1,2,3,4,5,6},juArrayList.toArray());
        juArrayList.setArray(new Integer[]{11,22,33,44,55,66});
        Assertions.assertArrayEquals(new Object[]{11,22,33,44,55,66},juArrayList.toArray());
        juArrayList.setArray(new Integer[]{100,200,300});
        Assertions.assertArrayEquals(new Object[]{100,200,300},juArrayList.toArray());
    }

    @Test
    void testToArray() {
        juArrayList.setArray(new Integer[]{1,2,3,4,5,6});
        Integer[] int_list = new Integer[juArrayList.size()];
        int_list=  juArrayList.toArray(int_list);
        Assertions.assertArrayEquals(int_list,juArrayList.toArray());

    }
}

