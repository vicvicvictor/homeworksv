package HomeworkFriday_12_08;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LinkedBagTest {
    LinkedBag<Integer> lstInteger = new LinkedBag<>();
    @Test
    void selectionSort() {
        lstInteger.add(10);
        lstInteger.add(3);
        lstInteger.add(7);
        lstInteger.add(5);
        lstInteger.add(2);
        lstInteger.add(0);
        Assertions.assertEquals("(6)root={data=0, sig->{data=2, sig->{data=5, sig->{data=7, sig->{data=3, sig->{data=10, sig->null}}}}}}", lstInteger.toString());
        lstInteger.selectionSort();
        Assertions.assertEquals("(6)root={data=0, sig->{data=2, sig->{data=3, sig->{data=5, sig->{data=7, sig->{data=10, sig->null}}}}}}",lstInteger.toString());
    }

    @Test
    void bubbleSort() {
        lstInteger.add(10);
        lstInteger.add(3);
        lstInteger.add(7);
        lstInteger.add(5);
        lstInteger.add(2);
        lstInteger.add(0);
        Assertions.assertEquals("(6)root={data=0, sig->{data=2, sig->{data=5, sig->{data=7, sig->{data=3, sig->{data=10, sig->null}}}}}}", lstInteger.toString());
        lstInteger.bubbleSort();
        Assertions.assertEquals("(6)root={data=0, sig->{data=2, sig->{data=3, sig->{data=5, sig->{data=7, sig->{data=10, sig->null}}}}}}",lstInteger.toString());
    }

}
