import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LinkedListIntTest {
    LinkedListInt linkedListInt = new LinkedListInt(1,2,3,4);
    @Test
    void add() {
        Assertions.assertEquals("[ 1 2 3 4 ]\n",linkedListInt.toString());
        linkedListInt.add(5);
        linkedListInt.add(6);
        linkedListInt.add(7);
        Assertions.assertEquals("[ 1 2 3 4 5 6 7 ]\n",linkedListInt.toString());
        linkedListInt.add(8);
        linkedListInt.add(9);
        Assertions.assertEquals("[ 1 2 3 4 5 6 7 8 9 ]\n",linkedListInt.toString());
    }

    @Test
    void get() {
        linkedListInt.add(5);
        linkedListInt.add(6);
        linkedListInt.add(7);
        Assertions.assertEquals(4,linkedListInt.get(3));
        Assertions.assertEquals(7,linkedListInt.get(6));
        Assertions.assertEquals(1,linkedListInt.get(0));
    }

    @Test
    void remove() {
        Assertions.assertEquals("[ 1 2 3 4 ]\n",linkedListInt.toString());
        linkedListInt.remove(1);
        Assertions.assertEquals("[ 2 3 4 ]\n",linkedListInt.toString());
        linkedListInt.remove(4);
        Assertions.assertEquals("[ 2 3 ]\n",linkedListInt.toString());
        linkedListInt.remove(3);
        Assertions.assertEquals("[ 2 ]\n",linkedListInt.toString());
    }

    @Test
    void isEmpty() {
        Assertions.assertEquals("[ 1 2 3 4 ]\n",linkedListInt.toString());
        assertFalse(linkedListInt.isEmpty());
        linkedListInt.remove(1);
        linkedListInt.remove(2);
        linkedListInt.remove(3);
        linkedListInt.remove(4);
        assertTrue(linkedListInt.isEmpty());
    }

    @Test
    void size() {
        Assertions.assertEquals("[ 1 2 3 4 ]\n",linkedListInt.toString());
        Assertions.assertEquals(4,linkedListInt.size());
        linkedListInt.add(5);
        linkedListInt.add(6);
        linkedListInt.add(7);
        Assertions.assertEquals(7,linkedListInt.size());
        linkedListInt.add(8);
        linkedListInt.add(9);
        Assertions.assertEquals(9,linkedListInt.size());
    }
}
