package Homework_Tuesday08_08;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class JULinkedListTest {
    List<String> listStrings = new JULinkedList<>();
    List<Integer> listIntegers = new JULinkedList<>();
    @Test
    void size() {
        /*------------------------------INTEGER LIST PART------------------------------------*/
        Assertions.assertEquals(0, listIntegers.size());
        listIntegers.add(1);
        listIntegers.add(2);
        Assertions.assertEquals(2, listIntegers.size());
        listIntegers.add(3);
        listIntegers.add(4);
        Assertions.assertEquals(4, listIntegers.size());

        /*------------------------------STRING LIST PART------------------------------------*/
        Assertions.assertEquals(0, listStrings.size());
        listStrings.add("1");
        listStrings.add("2");
        Assertions.assertEquals(2, listStrings.size());
        listStrings.add("3");
        listStrings.add("4");
        Assertions.assertEquals(4, listStrings.size());
        listStrings.add("1");
        Assertions.assertEquals(5, listStrings.size());
    }
    @Test
    void isEmpty() {
        /*------------------------------INTEGER LIST PART------------------------------------*/

        assertTrue(listIntegers.isEmpty());
        listIntegers.add(1);
        listIntegers.add(1);
        listIntegers.add(1);
        assertFalse(listIntegers.isEmpty());

        /*------------------------------STRING LIST PART------------------------------------*/
        assertTrue(listStrings.isEmpty());
        listStrings.add("1");
        listStrings.add("1");
        listStrings.add("1");
        assertFalse(listStrings.isEmpty());
    }

    @Test
    void contains() {
        /*------------------------------INTEGER LIST PART------------------------------------*/
        listIntegers.add(1);
        listIntegers.add(2);
        listIntegers.add(3);
        listIntegers.add(4);
        listIntegers.add(5);
        assertTrue(listIntegers.contains(1));
        assertTrue(listIntegers.contains(3));
        assertTrue(listIntegers.contains(4));
        assertTrue(listIntegers.contains(5));
        assertFalse(listIntegers.contains(10));
        assertFalse(listIntegers.contains(100));
        assertFalse(listIntegers.contains(1000));

        /*------------------------------STRING LIST PART------------------------------------*/
        listStrings.add("1");
        listStrings.add("2");
        listStrings.add("3");
        listStrings.add("4");
        listStrings.add("5");
        assertTrue(listStrings.contains("1"));
        assertTrue(listStrings.contains("3"));
        assertTrue(listStrings.contains("4"));
        assertTrue(listStrings.contains("5"));
        assertFalse(listStrings.contains("10"));
        assertFalse(listStrings.contains("100"));
        assertFalse(listStrings.contains("1000"));
    }

    @Test
    void iterator() {
        /*------------------------------INTEGER LIST PART------------------------------------*/
        int c =0;
        while (listIntegers.iterator().hasNext()){
            Assertions.assertEquals(listIntegers.get(c), listIntegers.iterator().next());
        }
        Assertions.assertEquals(0,c);

        /*------------------------------STRING LIST PART------------------------------------*/
        listStrings.add("1");
        listStrings.add("2");
        listStrings.add("3");
        listStrings.add("4");
        listStrings.add("5");
        int c2=0;
        for (String str :listStrings){
            Assertions.assertEquals(listStrings.get(c2), str);
            c2++;
        }
        Assertions.assertEquals(0,c);
    }

    @Test
    void add() {
        /*------------------------------INTEGER LIST PART------------------------------------*/
        Assertions.assertEquals(0, listIntegers.size());
        listIntegers.add(1);
        listIntegers.add(2);
        Assertions.assertEquals(2, listIntegers.size());
        listIntegers.add(3);
        listIntegers.add(4);
        Assertions.assertEquals(4, listIntegers.size());
        assertTrue(listIntegers.contains(1));
        assertTrue(listIntegers.contains(3));
        assertTrue(listIntegers.contains(4));
        assertTrue(listIntegers.contains(2));
        /*------------------------------STRING LIST PART------------------------------------*/
        Assertions.assertEquals(0, listStrings.size());
        listStrings.add("1");
        listStrings.add("2");
        Assertions.assertEquals(2, listStrings.size());
        listStrings.add("3");
        listStrings.add("4");
        Assertions.assertEquals(4, listStrings.size());
        listStrings.add("1");
        Assertions.assertEquals(5, listStrings.size());
        assertTrue(listStrings.contains("1"));
        assertTrue(listStrings.contains("3"));
        assertTrue(listStrings.contains("4"));
        assertTrue(listStrings.contains("2"));
    }

    @Test
    void remove() {
        /*------------------------------INTEGER LIST PART------------------------------------*/
        listIntegers.add(1);
        listIntegers.add(2);
        listIntegers.add(3);
        listIntegers.add(4);
        listIntegers.add(5);
        Assertions.assertEquals(5, listIntegers.size());
        assertTrue(listIntegers.contains(4));
        assertTrue(listIntegers.contains(5));
        listIntegers.remove(3);
        listIntegers.remove(3);
        Assertions.assertEquals(3, listIntegers.size());
        assertFalse(listIntegers.contains(4));
        assertFalse(listIntegers.contains(5));

        /*------------------------------STRING LIST PART------------------------------------*/
        listStrings.add("1");
        listStrings.add("2");
        listStrings.add("3");
        listStrings.add("4");
        listStrings.add("5");
        Assertions.assertEquals(5, listStrings.size());
        assertTrue(listStrings.contains("1"));
        assertTrue(listStrings.contains("2"));
        listStrings.remove(0);
        listStrings.remove(0);
        Assertions.assertEquals(3, listStrings.size());
        assertFalse(listStrings.contains("1"));
        assertFalse(listStrings.contains("2"));

    }

    @Test
    void clear() {
        /*------------------------------INTEGER LIST PART------------------------------------*/
        listIntegers.add(1);
        listIntegers.add(2);
        listIntegers.add(3);
        listIntegers.add(4);
        listIntegers.add(5);
        assertFalse(listIntegers.isEmpty());
        listIntegers.clear();
        assertTrue(listIntegers.isEmpty());

        /*------------------------------STRING LIST PART------------------------------------*/
        listStrings.add("1");
        listStrings.add("2");
        listStrings.add("3");
        listStrings.add("4");
        listStrings.add("5");
        assertFalse(listStrings.isEmpty());
        listStrings.clear();
        assertTrue(listStrings.isEmpty());
    }

    @Test
    void get() {
        /*------------------------------INTEGER LIST PART------------------------------------*/
        listIntegers.add(1);
        listIntegers.add(2);
        listIntegers.add(3);
        listIntegers.add(4);
        listIntegers.add(5);
        Assertions.assertEquals(1,listIntegers.get(0));
        Assertions.assertEquals(2,listIntegers.get(1));
        Assertions.assertEquals(3,listIntegers.get(2));
        Assertions.assertEquals(4,listIntegers.get(3));
        Assertions.assertEquals(5,listIntegers.get(4));

        /*------------------------------STRING LIST PART------------------------------------*/
        listStrings.add("1");
        listStrings.add("2");
        listStrings.add("3");
        listStrings.add("4");
        listStrings.add("5");
        Assertions.assertEquals("1", listStrings.get(0));
        Assertions.assertEquals("2", listStrings.get(1));
        Assertions.assertEquals("3", listStrings.get(2));
        Assertions.assertEquals("4", listStrings.get(3));
        Assertions.assertEquals("5", listStrings.get(4));
    }

    @Test
    void set() {
        /*------------------------------INTEGER LIST PART------------------------------------*/
        listIntegers.add(1);
        listIntegers.add(2);
        listIntegers.add(3);
        listIntegers.add(4);
        listIntegers.add(5);
        Assertions.assertEquals(1, listIntegers.get(0));
        Assertions.assertEquals(3, listIntegers.get(2));
        listIntegers.set(0,100);
        listIntegers.set(2,300);
        Assertions.assertEquals(100, listIntegers.get(0));
        Assertions.assertEquals(300, listIntegers.get(2));


        /*------------------------------STRING LIST PART------------------------------------*/
        listStrings.add("1");
        listStrings.add("2");
        listStrings.add("3");
        listStrings.add("4");
        listStrings.add("5");
        Assertions.assertEquals("1", listStrings.get(0));
        Assertions.assertEquals("5", listStrings.get(4));
        listStrings.set(0,"100");
        listStrings.set(4,"500");
        Assertions.assertEquals("100", listStrings.get(0));
        Assertions.assertEquals("500", listStrings.get(4));
    }

    @Test
    void testRemove() {
        /*------------------------------INTEGER LIST PART------------------------------------*/
        Integer n1 = 1;
        Integer n2 = 2;
        Integer n3 = 3;
        Integer n4 = 4;
        Integer n5 = 5;
        listIntegers.add(n1);
        listIntegers.add(n2);
        listIntegers.add(n3);
        listIntegers.add(n4);
        listIntegers.add(n5);
        Assertions.assertEquals(5, listIntegers.size());
        assertTrue(listIntegers.contains(n3));
        assertTrue(listIntegers.contains(n2));
        assertTrue(listIntegers.contains(n5));
        listIntegers.remove(n3);
        listIntegers.remove(n2);
        listIntegers.remove(n5);
        Assertions.assertEquals(2, listIntegers.size());
        //System.out.println(Arrays.toString(listIntegers.toArray()));
        assertFalse(listIntegers.contains(n3));
        assertFalse(listIntegers.contains(n2));
        assertFalse(listIntegers.contains(n5));
        /*------------------------------STRING LIST PART------------------------------------*/
        listStrings.add("1");
        listStrings.add("2");
        listStrings.add("3");
        listStrings.add("4");
        listStrings.add("5");
        Assertions.assertEquals(5, listStrings.size());
        assertTrue(listStrings.contains("1"));
        assertTrue(listStrings.contains("2"));
        assertTrue(listStrings.contains("5"));
        listStrings.remove("1");
        listStrings.remove("5");
        listStrings.remove("2");
        Assertions.assertEquals(2, listStrings.size());
        assertFalse(listStrings.contains("1"));
        assertFalse(listStrings.contains("2"));
        assertFalse(listStrings.contains("5"));
    }

    @Test
    void indexOf() {
        /*------------------------------INTEGER LIST PART------------------------------------*/
        listIntegers.add(1);
        listIntegers.add(2);
        listIntegers.add(3);
        listIntegers.add(4);
        listIntegers.add(5);
        listIntegers.add(1);
        listIntegers.add(2);
        listIntegers.add(3);
        listIntegers.add(4);
        listIntegers.add(5);
        listIntegers.add(1);
        listIntegers.add(2);
        listIntegers.add(3);
        listIntegers.add(4);
        listIntegers.add(5);
        Assertions.assertEquals(0,listIntegers.indexOf(1));
        Assertions.assertEquals(1,listIntegers.indexOf(2));
        Assertions.assertEquals(2,listIntegers.indexOf(3));
        Assertions.assertEquals(3,listIntegers.indexOf(4));
        Assertions.assertEquals(4,listIntegers.indexOf(5));

        /*------------------------------STRING LIST PART------------------------------------*/
        listStrings.add("1");
        listStrings.add("2");
        listStrings.add("3");
        listStrings.add("4");
        listStrings.add("5");
        listStrings.add("1");
        listStrings.add("2");
        listStrings.add("3");
        listStrings.add("4");
        listStrings.add("5");
        listStrings.add("1");
        listStrings.add("2");
        listStrings.add("3");
        listStrings.add("1");
        listStrings.add("2");
        listStrings.add("3");
        listStrings.add("4");
        listStrings.add("5");
        listStrings.add("4");
        listStrings.add("5");
        Assertions.assertEquals(0,listStrings.indexOf("1"));
        Assertions.assertEquals(1,listStrings.indexOf("2"));
        Assertions.assertEquals(2,listStrings.indexOf("3"));
        Assertions.assertEquals(3,listStrings.indexOf("4"));
        Assertions.assertEquals(4,listStrings.indexOf("5"));
    }

    @Test
    void lastIndexOf() {
        /*------------------------------INTEGER LIST PART------------------------------------*/
        listIntegers.add(1);
        listIntegers.add(2);
        listIntegers.add(3);
        listIntegers.add(4);
        listIntegers.add(2);
        listIntegers.add(3);
        listIntegers.add(4);
        listIntegers.add(5);
        listIntegers.add(1);
        listIntegers.add(5);
        Assertions.assertEquals(8,listIntegers.lastIndexOf(1));
        Assertions.assertEquals(4,listIntegers.lastIndexOf(2));
        Assertions.assertEquals(5,listIntegers.lastIndexOf(3));
        Assertions.assertEquals(6,listIntegers.lastIndexOf(4));
        Assertions.assertEquals(9,listIntegers.lastIndexOf(5));

        /*------------------------------STRING LIST PART------------------------------------*/
        listStrings.add("1");
        listStrings.add("2");
        listStrings.add("3");
        listStrings.add("4");
        listStrings.add("2");
        listStrings.add("3");
        listStrings.add("4");
        listStrings.add("5");
        listStrings.add("1");
        listStrings.add("5");
        Assertions.assertEquals(8,listStrings.lastIndexOf("1"));
        Assertions.assertEquals(4,listStrings.lastIndexOf("2"));
        Assertions.assertEquals(5,listStrings.lastIndexOf("3"));
        Assertions.assertEquals(6,listStrings.lastIndexOf("4"));
        Assertions.assertEquals(9,listStrings.lastIndexOf("5"));
    }

    @Test
    void toArray() {
        /*------------------------------INTEGER LIST PART------------------------------------*/
        listIntegers.add(1);
        listIntegers.add(2);
        listIntegers.add(3);
        listIntegers.add(4);
        listIntegers.add(5);
        Assertions.assertArrayEquals(new Integer[]{1,2,3,4,5}, listIntegers.toArray());

        /*------------------------------STRING LIST PART------------------------------------*/
        listStrings.add("1");
        listStrings.add("2");
        listStrings.add("3");
        listStrings.add("4");
        listStrings.add("5");
        Assertions.assertArrayEquals(new String[]{"1","2","3","4","5"}, listStrings.toArray());
    }

}

