import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.assertEquals;

import static org.junit.Assert.assertArrayEquals;

class GameControllerTest {
    GameController gameController = new GameController();
    @Test
    void getObstaclesSize() {
        gameController.fillWithObstacles();
        gameController.setPlayerPosition();
        int numObstacles = gameController.getObstaclesSize();
        Assertions.assertEquals(5, numObstacles);
    }
    @Test
    void moveUpPlayerPositionOnBoard() {
        gameController.fillForTesting();
        gameController.moveUpPlayerPositionOnBoard();
        Assertions.assertEquals("·X···0····\n" +
                "··········\n" +
                "······0···\n" +
                "··········\n" +
                "··········\n" +
                "··0·······\n" +
                "··········\n" +
                "·····0····\n" +
                "··········\n" +
                "········0·\n", gameController.getBoardOnString());
    }

    @Test
    void moveDownPlayerPositionOnBoard() {
        gameController.fillForTesting();
        gameController.moveDownPlayerPositionOnBoard();
        Assertions.assertEquals("·····0····\n" +
                "··········\n" +
                "·X····0···\n" +
                "··········\n" +
                "··········\n" +
                "··0·······\n" +
                "··········\n" +
                "·····0····\n" +
                "··········\n" +
                "········0·\n", gameController.getBoardOnString());
    }

    @Test
    void moveRightPlayerPositionOnBoard() {
        gameController.fillForTesting();
        gameController.moveRightPlayerPositionOnBoard();
        Assertions.assertEquals("·····0····\n" +
                "··X·······\n" +
                "······0···\n" +
                "··········\n" +
                "··········\n" +
                "··0·······\n" +
                "··········\n" +
                "·····0····\n" +
                "··········\n" +
                "········0·\n", gameController.getBoardOnString());
    }

    @Test
    void moveLeftPlayerPositionOnBoard() {
        gameController.fillForTesting();
        gameController.moveLeftPlayerPositionOnBoard();
        Assertions.assertEquals("·····0····\n" +
                "X·········\n" +
                "······0···\n" +
                "··········\n" +
                "··········\n" +
                "··0·······\n" +
                "··········\n" +
                "·····0····\n" +
                "··········\n" +
                "········0·\n", gameController.getBoardOnString());
    }
}