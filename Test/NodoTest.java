import static org.junit.Assert.*;
import org.junit.Test;
import static org.junit.Assert.assertNull;

import org.junit.Test;
public class NodoTest {

    @Test
    public void test0() {
        Nodo n = new Nodo();
        n.data = 1337;
        try{
            assertEquals(Nodo.getNth(n, 0), 1337);
        }catch(Exception e){
            assertTrue(false);
        }
    }

    @Test
    public void test2() {
        Nodo n = new Nodo();
        n.data = 1337;
        n.next = new Nodo();
        n.next.data = 42;
        n.next.next = new Nodo();
        n.next.next.data = 23;
        try{
            assertEquals(Nodo.getNth(n, 0), 1337);
            assertEquals(Nodo.getNth(n, 1), 42);
            assertEquals(Nodo.getNth(n, 2), 23);
        }catch(Exception e){
            assertTrue(false);
        }
    }

    @Test
    public void testNull() {
        try{
            Nodo.getNth(null, 0);
            assertTrue(false);
        }catch(Exception e){
            assertTrue(true);
        }
    }

    @Test
    public void testOther() {
        Nodo n = new Nodo();
        n.data = 0;
        n.next = new Nodo();
        n.next.data = 1;
        n.next.next = new Nodo();
        n.next.next.data = -1;

        try{
            try{
                Nodo.getNth(n, -1);
                assertTrue(false);
            }catch(Exception e){
                assertTrue(true);
            }
            try{
                Nodo.getNth(n, -1337);
                assertTrue(false);
            }catch(Exception e){
                assertTrue(true);
            }
            assertEquals(Nodo.getNth(n, 0),0);
            assertEquals(Nodo.getNth(n, 1),1);
            assertEquals(Nodo.getNth(n, 2),-1);
            try{
                Nodo.getNth(n, 3);
                assertTrue(false);
            }catch(Exception e){
                assertTrue(true);
            }
            try{
                Nodo.getNth(n, 1337);
                assertTrue(false);
            }catch(Exception e){
                assertTrue(true);
            }
        }catch(Exception e){
            assertTrue(false);
        }
    }

    @Test
    public void testWrongIdx() {
        try{
            Nodo.getNth(new Nodo(), 1);
            assertTrue(false);
        }catch(Exception e){
            assertTrue(true);
        }
    }

/*-----------------------------------------------------------------------------------------------------*/
    @Test
    public void twoEmpty() throws Exception {
        assertNull( Nodo.append( null, null ) );
    }

    @Test
    public void oneEmpty() throws Exception {
        assertEquals( Nodo.append( null, new Nodo( 1 ) ).toString() , new Nodo( 1 ).toString() );
        assertEquals( Nodo.append( new Nodo( 1 ), null ).toString(), new Nodo( 1 ).toString() );
    }

    @Test
    public void oneOne() throws Exception {
        Nodo n = new Nodo(1);
        n.next = new Nodo(2);
        Nodo n2 = new Nodo(2);
        n2.next = new Nodo(1);

        assertEquals( Nodo.append( new Nodo( 1 ), new Nodo( 2 ) ).toString(), n.toString() );
        assertEquals( Nodo.append( new Nodo( 2 ), new Nodo( 1 ) ).toString() ,n2.toString());
    }

    @Test
    public void bigLists() throws Exception {
        Nodo n = new Nodo(1);
        n.next = new Nodo(2);
        Nodo n2 = new Nodo(3);
        n2.next = new Nodo(4);
        Nodo n3 = new Nodo(1);
        Nodo n31= new Nodo(2);
        Nodo n32= new Nodo(3);
        Nodo n33= new Nodo(4);
        n3.next=n31;
        n31.next=n32;
        n32.next=n33;
        assertEquals(Nodo.append( n,n2 ).toString(),n3.toString() );


        Nodo x = new Nodo(1);
        Nodo x1 = new Nodo(2);
        Nodo x2 = new Nodo(3);
        Nodo x3 = new Nodo(4);
        Nodo x4 = new Nodo(5);
        x.next = x1;
        x1.next=x2;
        x2.next=x3;
        x3.next =x4;

        Nodo y = new Nodo(6);
        Nodo y1 = new Nodo(7);
        Nodo y2 = new Nodo(8);
        y.next=y1;
        y1.next=y2;

        Nodo v = new Nodo(1);
        Nodo v1 = new Nodo(2);
        Nodo v2 = new Nodo(3);
        Nodo v3 = new Nodo(4);
        Nodo v4 = new Nodo(5);
        Nodo v5 = new Nodo(6);
        Nodo v6 = new Nodo(7);
        Nodo v7 = new Nodo(8);

        v.next=v1;
        v1.next=v2;
        v2.next=v3;
        v3.next=v4;
        v4.next=v5;
        v5.next=v6;
        v6.next=v7;




        assertEquals(Nodo.append( x, y).toString(), v.toString());

        
    }
}