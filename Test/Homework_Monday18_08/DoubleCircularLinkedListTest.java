package Homework_Monday18_08;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DoubleCircularLinkedListTest {
    List<Integer> listInteger = new DoubleCircularLinkedList<>();
    List<String> listString = new DoubleCircularLinkedList<>();
    @Test
    void size() {
        /*
        TEST FOR INTEGER
         */

        assertEquals(0,listInteger.size());
        listInteger.add(1);
        listInteger.add(2);
        listInteger.add(3);
        assertEquals(3,listInteger.size());
        listInteger.add(4);
        listInteger.add(5);
        listInteger.add(6);
        listInteger.add(7);
        assertEquals(7,listInteger.size());
        listInteger.remove(1);
        listInteger.remove(2);
        assertEquals(5,listInteger.size());
        listInteger.remove(3);
        listInteger.remove(4);
        listInteger.remove(5);
        listInteger.remove(6);
        assertEquals(1,listInteger.size());

        /*
        TEST FOR STRING
         */

        assertEquals(0, listString.size());
        listString.add("1");
        assertEquals(1, listString.size());
        listString.add("2");
        listString.add("3");
        listString.add("4");
        assertEquals(4, listString.size());
        listString.add("5");
        listString.add("6");
        listString.add("7");
        listString.add("8");
        listString.remove("8");
        assertEquals(7, listString.size());
        listString.remove("3");
        listString.remove("1");
        listString.remove("5");
        assertEquals(4, listString.size());

    }

    @Test
    void isEmpty() {
        /*
        TEST FOR INTEGER
         */
        assertTrue(listInteger.isEmpty());
        listInteger.add(1);
        listInteger.add(2);
        listInteger.add(3);
        listInteger.add(4);
        assertFalse(listInteger.isEmpty());
        /*
        TEST FOR STRING
         */
        assertTrue(listString.isEmpty());
        listString.add("1");
        listString.add("2");
        listString.add("3");
        listString.add("4");
        assertFalse(listString.isEmpty());
    }
    @Test
    void add(){
        /*
        TEST FOR INTEGER
         */
        assertTrue(listInteger.isEmpty());
        listInteger.add(1);
        assertFalse(listInteger.isEmpty());
        assertEquals(1, listInteger.get(0));
        listInteger.add(2);
        listInteger.add(3);
        listInteger.add(4);
        assertEquals(2, listInteger.get(1));
        assertEquals(3, listInteger.get(2));
        assertEquals(4, listInteger.get(3));
        assertEquals(1, listInteger.get(4));
        assertEquals(2, listInteger.get(5));
        assertEquals(3, listInteger.get(6));

        listInteger.add(5);
        assertEquals(5, listInteger.get(4));
        assertEquals(5, listInteger.get(9));
        assertEquals(5, listInteger.get(14));
        assertEquals(5, listInteger.get(19));
        assertEquals(5, listInteger.get(24));

        /*
        TEST FOR STRING
         */
        assertTrue(listString.isEmpty());
        listString.add("1");
        assertFalse(listString.isEmpty());
        assertEquals("1", listString.get(0));
        listString.add("2");
        listString.add("3");
        listString.add("4");
        assertEquals("2", listString.get(1));
        assertEquals("3", listString.get(2));
        assertEquals("4", listString.get(3));
        assertEquals("1", listString.get(4));
        assertEquals("2", listString.get(5));
        assertEquals("3", listString.get(6));

        listString.add("5");
        assertEquals("5", listString.get(4));
        assertEquals("5", listString.get(9));
        assertEquals("5", listString.get(14));
        assertEquals("5", listString.get(19));
        assertEquals("5", listString.get(24));
    }

    @Test
    void remove() {
        /*
        TEST FOR INTEGER
         */
        assertTrue(listInteger.isEmpty());
        listInteger.add(1);
        assertFalse(listInteger.isEmpty());
        assertEquals(1, listInteger.get(0));
        listInteger.add(2);
        listInteger.add(3);
        listInteger.add(4);
        assertEquals(2, listInteger.get(1));
        listInteger.remove(2);
        assertEquals(3,listInteger.get(1));
        listInteger.remove(3);
        assertEquals(4,listInteger.get(1));
        listInteger.add(5);
        listInteger.add(6);
        listInteger.add(7);
        listInteger.add(8);
        assertEquals(6, listInteger.size());
        assertEquals(5, listInteger.get(2));
        listInteger.remove(5);
        assertEquals(6,listInteger.get(2));
        listInteger.remove(6);
        assertEquals(7,listInteger.get(2));
        assertEquals(4, listInteger.size());



        /*
        TEST FOR STRING
         */
        assertTrue(listString.isEmpty());
        listString.add("1");
        assertFalse(listString.isEmpty());
        assertEquals("1", listString.get(0));
        listString.add("2");
        listString.add("3");
        listString.add("4");
        assertEquals("2", listString.get(1));
        listString.remove("2");
        assertEquals("3",listString.get(1));
        listString.remove("3");
        assertEquals("4",listString.get(1));
        listString.add("5");
        listString.add("6");
        listString.add("7");
        listString.add("8");
        assertEquals(6, listString.size());
        assertEquals("5", listString.get(2));
        listString.remove("5");
        assertEquals("6",listString.get(2));
        listString.remove("6");
        assertEquals("7",listString.get(2));
        assertEquals(4, listString.size());
    }

    @Test
    void get() {
        /*
        TEST FOR INTEGER
         */
        assertTrue(listInteger.isEmpty());
        listInteger.add(10);
        assertFalse(listInteger.isEmpty());
        assertEquals(10, listInteger.get(0));
        listInteger.add(20);
        listInteger.add(30);
        listInteger.add(40);
        assertEquals(20, listInteger.get(1));
        listInteger.remove(20);
        assertEquals(30,listInteger.get(1));
        listInteger.remove(30);
        assertEquals(40,listInteger.get(1));
        listInteger.add(50);
        listInteger.add(60);
        listInteger.add(70);
        listInteger.add(80);
        assertEquals(6, listInteger.size());
        assertEquals(50, listInteger.get(2));
        listInteger.remove(50);
        assertEquals(60,listInteger.get(2));
        listInteger.remove(60);
        assertEquals(70,listInteger.get(2));
        assertEquals(4, listInteger.size());

        /*
        TEST FOR STRING
         */
        assertTrue(listString.isEmpty());
        listString.add("HELLO");
        assertFalse(listString.isEmpty());
        assertEquals("HELLO", listString.get(0));
        listString.add("MY");
        listString.add("NAME");
        listString.add("IS");
        assertEquals("MY", listString.get(1));
        listString.remove("MY");
        assertEquals("NAME",listString.get(1));
        listString.remove("NAME");
        assertEquals("IS",listString.get(1));
        listString.add("A");
        listString.add("D");
        listString.add("a");
        listString.add("M");
        assertEquals(6, listString.size());
        assertEquals("A", listString.get(2));
        listString.remove("A");
        assertEquals("D",listString.get(2));
        listString.remove("D");
        assertEquals("a",listString.get(2));
        assertEquals(4, listString.size());
    }

    @Test
    void xchange(){

        /*
        TEST FOR INTEGER
         */

        listInteger.add(1);
        listInteger.add(2);
        listInteger.add(3);
        listInteger.add(4);
        listInteger.add(5);
        listInteger.add(6);
        listInteger.add(7);
        listInteger.add(8);
        listInteger.add(9);
        listInteger.add(10);
        assertEquals(10, listInteger.size());
        assertEquals(3, listInteger.get(2));
        assertEquals(7, listInteger.get(6));

        listInteger.xchange(6,2);

        assertEquals(7, listInteger.get(2));
        assertEquals(3, listInteger.get(6));
        assertEquals(10, listInteger.size());

        /*************************************************/


        assertEquals(10, listInteger.get(9));
        assertEquals(6, listInteger.get(5));

        listInteger.xchange(9,5);

        assertEquals(6, listInteger.get(9));
        assertEquals(10, listInteger.get(5));


        /***************************************************/


        assertEquals(5, listInteger.get(4));
        assertEquals(2, listInteger.get(1));

        listInteger.xchange(4,1);

        assertEquals(2, listInteger.get(4));
        assertEquals(5, listInteger.get(1));




        /*
        TEST FOR STRING
         */


        listString.add("1");
        listString.add("2");
        listString.add("3");
        listString.add("4");
        listString.add("5");
        listString.add("6");
        listString.add("7");
        listString.add("8");
        listString.add("9");
        listString.add("10");
        assertEquals(10, listString.size());
        assertEquals("3", listString.get(2));
        assertEquals("7", listString.get(6));

        listString.xchange(6,2);

        assertEquals("7", listString.get(2));
        assertEquals("3", listString.get(6));
        assertEquals(10, listString.size());

        /*************************************************/


        assertEquals("10", listString.get(9));
        assertEquals("6", listString.get(5));

        listString.xchange(9,5);

        assertEquals("6", listString.get(9));
        assertEquals("10", listString.get(5));


        /***************************************************/


        assertEquals("5", listString.get(4));
        assertEquals("2", listString.get(1));

        listString.xchange(4,1);

        assertEquals("2", listString.get(4));
        assertEquals("5", listString.get(1));


    }

}