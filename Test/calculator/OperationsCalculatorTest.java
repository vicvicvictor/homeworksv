package calculator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OperationsCalculatorTest {

    @Test
    public void basicSum() throws Exception {
        AbstractCalc calc = new Calculator();
        int actual = calc.calc("100+10");
        int expected = 110;

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void sum() throws Exception {
        AbstractCalc calc = new Calculator();
        int actual = calc.calc("2147483647+0");
        int expected = 2147483647;

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void sum2() throws Exception {
        AbstractCalc calc = new Calculator();
        int actual = calc.calc("21474836+0 +110+1+2");
        int expected = 21474949;

        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void sum3() throws Exception {
        AbstractCalc calc = new Calculator();
        int actual = calc.calc("1+2+3+4+5+6+7+8+9+10");
        int expected = 55;

        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void basicMultiplication() throws Exception {
        AbstractCalc calc = new Calculator();
        int actual = calc.calc("3*2");
        int expected = 6;

        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void Multiplication() throws Exception {
        AbstractCalc calc = new Calculator();
        int actual = calc.calc("3*3");
        int expected = 9;

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void Multiplication2() throws Exception {
        AbstractCalc calc = new Calculator();
        int actual = calc.calc("100*100*100");
        int expected = 1000000;

        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void Multiplication3() throws Exception {
        AbstractCalc calc = new Calculator();
        int actual = calc.calc("32*245*121*10");
        int expected = 9486400;

        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void Multiplication4() throws Exception {
        AbstractCalc calc = new Calculator();
        int actual = calc.calc("31243*1212413*0*342*145341*0");
        int expected = 0;

        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void power() throws Exception {
        AbstractCalc calc = new Calculator();
        int actual = calc.calc("2^5");
        int expected = 32;

        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void power1() throws Exception {
        AbstractCalc calc = new Calculator();
        int actual = calc.calc("10^3");
        int expected = 1000;

        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void power2() throws Exception {
        AbstractCalc calc = new Calculator();
        int actual = calc.calc("5^3");
        int expected = 125;

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void factorial1() throws Exception {
        AbstractCalc calc = new Calculator();
        int actual = calc.calc("1!");
        int expected = 1;

        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void factorial2() throws Exception {
        AbstractCalc calc = new Calculator();
        int actual = calc.calc("2!");
        int expected = 2;

        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void factorial3() throws Exception {
        AbstractCalc calc = new Calculator();
        int actual = calc.calc("5!");
        int expected = 120;

        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void factorial4() throws Exception {
        AbstractCalc calc = new Calculator();
        int actual = calc.calc("10!");
        int expected = 3628800;

        Assertions.assertEquals(expected, actual);
    }

}