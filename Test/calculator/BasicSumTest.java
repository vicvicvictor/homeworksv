package calculator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

class BasicSumTest {

    @Test
    public void twoOperantsSumTest() throws Exception {
        AbstractCalc calc = new Calculator();
        int actual = calc.calc("1+1");
        int expected = 2;

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void severalOperantsSumTest() throws Exception {
        AbstractCalc calc = new Calculator();
        int actual = calc.calc("1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1+1");
        int expected = 32;

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void largeIntsOperantsSumTest() throws Exception {
        AbstractCalc calc = new Calculator();
        int actual = calc.calc("2000000+1");
        int expected = 2000001;

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void validSpacesOperantsSumTest() throws Exception {
        AbstractCalc calc = new Calculator();
        int actual = calc.calc("  1+1");
        int expected = 1;

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void moreValidSpacesOperantsSumTest() throws Exception {
        AbstractCalc calc = new Calculator();
        int actual = calc.calc(" 1+ 1");
        int expected = 2;

        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void severalOperantsAndSpacesSumTest() throws Exception {
        AbstractCalc calc = new Calculator();
        int actual = calc.calc(" 1+ 1+1+1+1+1 +1 +1+1+1 +1");
        int expected = 10;

        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void twoOperantsSumTestB() throws Exception {
        AbstractCalc calc = new Calculator();
        int actual = calc.calc("(1+1)");
        int expected = 2;

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void moreBracketsOperantsSumTest() throws Exception {
        AbstractCalc calc = new Calculator();
        int actual = calc.calc("(1+1+1+(1+1))");
        int expected = 4;

        Assertions.assertEquals(expected, actual);
    }

}