package calculator;

import calculator.exceptions.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorOperationExceptionsTest {
    @Test
    public void validSumTest() throws Exception{
        AbstractCalc calc = new Calculator();
        int actual = calc.calc("1+1");
        int expected = 2;

        assertEquals(expected, actual);
    }

    @Test
    public void invalidTokenSumTest() {
        AbstractCalc calc = new Calculator();
        Exception ex = assertThrows(InvalidOperationException.class, () -> calc.calc("1-1"));
        String expected = "The operation -> '-' is invalid. Be careful";
        String actual = ex.getMessage();

        assertEquals(expected, actual);
    }

    @Test
    public void malformedTokenSumTest() {
        AbstractCalc calc = new Calculator();
        Exception ex = assertThrows(InvalidImcompleteOperationStructureException.class, () -> calc.calc("1++1"));

        String expected = "Please write a correct Operation. Be careful";
        String actual = ex.getMessage();

        assertEquals(expected, actual);
    }

    @Test
    public void incompledOperanExpectedTokenSumTest() {
        AbstractCalc calc = new Calculator();
        Exception ex = assertThrows(InvalidImcompleteOperationStructureException.class, () -> calc.calc("1+1+"));

        String expected = "Please write a correct Operation. Be careful";
        String actual = ex.getMessage();

        assertEquals(expected, actual);
    }

    @Test
    public void missingCloseBracketTokenSumTest() {
        AbstractCalc calc = new Calculator();
        Exception ex = assertThrows(MissingParanthesisException.class, () -> calc.calc("(1+1+1"));

        String expected = "The operation is missing parenthesis. Be careful.";
        String actual = ex.getMessage();

        assertEquals(expected, actual);
    }

    @Test
    public void GiganticOperationResultExceptionTest() {
        AbstractCalc calc = new Calculator();
        Exception ex = assertThrows(GiganticOperationResultException.class, () -> calc.calc("2147483647+1"));

        String expected = "The result is greater than 2147483647. Be careful";
        String actual = ex.getMessage();

        assertEquals(expected, actual);
    }
}

