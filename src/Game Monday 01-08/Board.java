import View.GameMessages;

public class Board {

    private String[][] board;
    private final String charfreeSpaces ="·";
    private GameMessages boardMessages;
    public Board(int n){
        board=new String[n][n];
        boardMessages= new GameMessages();
        fillBoard();
    }

    public String[][] getBoard() {
        return board;
    }
    public String getCharfreeSpaces(){
        return charfreeSpaces;
    }

    public void setBoard(String[][] board) {
        this.board = board;
    }
    public void fillBoard(){
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                board[i][j] = charfreeSpaces;
            }
        }
    }
    public void printBoard(){
        System.out.println("");
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                System.out.print(board[i][j]+" ");
            }
            System.out.print("\n");

        }
    }
    public String getBoardOnString(){
        StringBuilder boardS = new StringBuilder();
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                boardS.append(board[i][j]);
            }
            boardS.append("\n");
        }
        return boardS.toString();
    }
    public int getBoardTotalSize(){
        int counter =0;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                counter++;
            }
        }
        return counter;
    }
    public int getSize(){
        return board.length;
    }
    public void setPosition(int x, int y, String string){
        board[x][y] = string;
    }
    public String getItem(int x, int y){
        return board[x][y];
    }
    public boolean verifyIfPostionLegal(int x, int y, int move){
        boolean isMoveLegal= true;
        switch (move){
            case 1:
                if (x == 0){
                    isMoveLegal=false;
                    boardMessages.printMessagePositionIlligalUp();
                }
                break;
            case 2:
                if (x == getSize()-1){
                    isMoveLegal=false;
                    boardMessages.printMessagePositionIlligalDown();
                }
                break;
            case 3:
                if (y==getSize()-1){
                    isMoveLegal=false;
                    boardMessages.printMessagePositionIlligaRight();
                }
                break;
            case 4:
                if(y ==0){
                    isMoveLegal=false;
                    boardMessages.printMessagePositionIlligalLeft();
                }
                break;
            }
            return isMoveLegal;
        }
}
