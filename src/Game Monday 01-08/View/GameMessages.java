package View;

public class GameMessages {

    public void printMessagePositionIlligalUp(){
        System.out.println("You can not go higher because you are already on the top of the board");
    }
    public void printMessagePositionIlligalDown(){
        System.out.println("You can not go any lower because you are already on the top of the board");
    }
    public void printMessagePositionIlligaRight(){
        System.out.println("You can not go further to the right because you are already on the top of the board");
    }
    public void printMessagePositionIlligalLeft(){
        System.out.println("You can not go further to the left because you are already on the top of the board");
    }
    public void printMessageIsAnObstacle(){
        System.out.println("You hit an obstacle. Open your eyes.");
    }
    public void printWelcomeToGame(){
        System.out.println("WELCOMEEEE TO THE GAME <THE MAP>");
    }
    public void printGameInstruccions(String name){
        System.out.println("Hello player "+name+", this are the game instruccions:\n"+
                "1. Your goal is to get to the bottom right corner without touching any obstacles\n"+
                "2. You can move with numbers.\n"+
                "3. Press 1 to go up\n"+
                "4. Press 2 to go down\n"+
                "5. Press 3 to go to the right\n"+
                "6. Press 4 to go to the left\n"+
                "GOOD LUCK, "+ name+".");
    }
    public void printEnterName(){
        System.out.println("Please, enter your name: ");
    }
    public void printCongratulations(){
        System.out.println("CONGRATULATIONS, you won!!");
    }
}
