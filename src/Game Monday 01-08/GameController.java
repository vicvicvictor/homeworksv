import View.GameMessages;

import java.util.Random;
import java.util.Scanner;


public class GameController {
    private Board board;
    private Player player;
    private final Random random = new Random();
    private GameMessages gameMessages;
    private final Scanner scanner = new Scanner(System.in);
    private int numberObstacles;
    private final String charObstacle = "0";
    private final String charPlayer ="X";
    public GameController(){
        board= new Board(10);
        player= new Player();
        gameMessages = new GameMessages();
        numberObstacles =5;
    }

    public void fillWithObstacles(){
        while (numberObstacles>0){
            int posX = random.nextInt(board.getSize());
            int posY = random.nextInt(board.getSize());
            if (board.getItem(posX,posY).equals(charObstacle)){
                numberObstacles++;
            }
            board.setPosition(posX,posY, charObstacle);
            board.getItem(posX,posY);
            numberObstacles--;
        }
    }

    public void setPlayerPosition(){
        if ( player.getPosY() != null && player.getPosX() != null){
            board.setPosition(player.getPosX(), player.getPosY(), charObstacle);
        }
        int posX= random.nextInt(board.getSize());
        int posY= random.nextInt(board.getSize());
        board.setPosition(posX,posY,charPlayer);
        player.setPosX(posX);
        player.setPosY(posY);
    }
    public int getObstaclesSize(){
        int n=0;
        for (int i = 0; i < board.getSize(); i++) {
            for (int j = 0; j < board.getBoard()[i].length; j++) {
                if (board.getItem(i,j).equals(charObstacle)){
                    n++;
                }
            }
        }
        return n;
    }
    public void moveUpPlayerPositionOnBoard(){
        int posX = player.getPosX();
        int posY = player.getPosY();
        if(board.verifyIfPostionLegal(posX,posY,1) && isAnObstacle(posX - 1, posY)) {
            board.setPosition(player.getPosX(), player.getPosY(), board.getCharfreeSpaces());
            player.sumPosX(-1);
            board.setPosition(player.getPosX(), player.getPosY(), charPlayer);
        }
        }

    public void moveDownPlayerPositionOnBoard(){
        int posX = player.getPosX();
        int posY = player.getPosY();
        if(board.verifyIfPostionLegal(posX,posY,2) && isAnObstacle(posX + 1, posY)) {
            board.setPosition(player.getPosX(), player.getPosY(), board.getCharfreeSpaces());
            player.sumPosX(1);
            board.setPosition(player.getPosX(), player.getPosY(), charPlayer);
        }
    }
    public void moveRightPlayerPositionOnBoard(){
        int posX = player.getPosX();
        int posY = player.getPosY();
        if(board.verifyIfPostionLegal(posX,posY,3) && isAnObstacle(posX, posY + 1)) {
            board.setPosition(player.getPosX(), player.getPosY(), board.getCharfreeSpaces());
            player.sumPosY(1);
            board.setPosition(player.getPosX(), player.getPosY(), charPlayer);
        }
    }
    public void moveLeftPlayerPositionOnBoard(){
        int posX = player.getPosX();
        int posY = player.getPosY();
        if(board.verifyIfPostionLegal(posX,posY,4) && isAnObstacle(posX, posY - 1)) {
            board.setPosition(player.getPosX(), player.getPosY(), board.getCharfreeSpaces());
            player.sumPosY(-1);
            board.setPosition(player.getPosX(), player.getPosY(), charPlayer);
        }
    }
    private boolean isAnObstacle(int x, int y){
        boolean isObstacle=false;
        if (board.getItem(x,y).equals(charObstacle)){
            isObstacle=true;
            gameMessages.printMessageIsAnObstacle();
        }
        return !isObstacle;
    }

    public boolean win(){
        return player.getPosX() == 9 && player.getPosY() == 9;
    }
    public void execute(){
        gameMessages.printWelcomeToGame();
        gameMessages.printEnterName();
        gameMessages.printGameInstruccions(player.getName());
        fillWithObstacles();
        setPlayerPosition();
        board.printBoard();
        while (!win()){
            int move = scanner.nextInt();
            switch (move) {
                case 1 -> moveUpPlayerPositionOnBoard();
                case 2 -> moveDownPlayerPositionOnBoard();
                case 3 -> moveRightPlayerPositionOnBoard();
                case 4 -> moveLeftPlayerPositionOnBoard();
            }
            board.printBoard();
        }
        gameMessages.printCongratulations();
    }

    public void fillForTesting(){
        board.setPosition(0,5,charObstacle);
        board.setPosition(2,6,charObstacle);
        board.setPosition(5,2,charObstacle);
        board.setPosition(7,5,charObstacle);
        board.setPosition(9,8,charObstacle);
        board.setPosition(1,1,charPlayer);
        player.setPosX(1);
        player.setPosY(1);
    }
    public String getBoardOnString(){
        return board.getBoardOnString();
    }
}


