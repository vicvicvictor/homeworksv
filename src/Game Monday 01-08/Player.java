import java.util.Scanner;

public class Player {
    private String name;
    private Integer posX;
    private Integer posY;
    private Scanner scanner = new Scanner(System.in);
    public void setName(String name) {
        this.name = name;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }


    public String getName() {

        setName(scanner.next());
        return name;
    }

    public Integer getPosX() {
        return posX;
    }

    public Integer getPosY() {
        return posY;
    }

    public void sumPosX(int i){
        posX= posX + i;
    }
    public void sumPosY(int i){
        posY= posY + i;
    }

}
