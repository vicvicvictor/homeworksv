package homeworkOnClass;

public class UtilMethodsQueue {
    public static Queue intercalarQueues(Queue queue1, Queue queue2){
        int totalSize = queue1.size()+queue2.size();
        Queue newQueue = new Queue();
        for (int i = 1; i <= totalSize; i++) {
            if (i%2 ==0){
                newQueue.enqueue(queue2.dequeue());
            }else{
                newQueue.enqueue(queue1.dequeue());
            }
        }
        return newQueue;
    }
}
