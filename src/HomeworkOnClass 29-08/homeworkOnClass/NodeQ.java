package homeworkOnClass;

public class NodeQ {
    private int data;
    private NodeQ next;
    public NodeQ(int data){
        this.data =data;
    }
    public int getData(){
        return data;
    }
    public NodeQ getNext(){
        return next;
    }
    public void setNext(NodeQ nodeQ){
        this.next = nodeQ;
    }
}
