package homeworkOnClass;

public class Queue{
    private NodeQ head;
    private NodeQ tail;
    private int size;
    public Queue(){this.size=0;}
    public int size(){
        return size;
    }
    public boolean isEmpty(){
        return  size==0;
    }
    public int getHeadData(){
        return head.getData();
    }
    public int getTailData(){
        return tail.getData();
    }
    public void enqueue(int element){
        NodeQ newElement = new NodeQ(element);
        if (size ==0){
            tail = newElement;
            head = tail;
        }else {
            newElement.setNext(tail);
            tail = newElement;
        }
        size++;
    }
    private void reboot(){
        tail =null;
        head = null;
    }
    public int dequeue(){
        NodeQ currentNode = tail;
        int elementRemoved;
        if (size ==1){
            reboot();
            elementRemoved = currentNode.getData();
        }else {
            while (currentNode.getNext()!=head){
                currentNode=currentNode.getNext();
            }
            elementRemoved=currentNode.getNext().getData();
            currentNode.setNext(null);
            head=currentNode;
        }
        size--;
        return elementRemoved;
    }

    public void display(){
        int i =0;
        for (NodeQ currentNode = tail; currentNode!=null;currentNode = currentNode.getNext()){

            System.out.println(i + ".) "+currentNode.getData());
            i++;
        }
        System.out.println("tail: "+getTailData());
        System.out.println("head: "+getHeadData());
        System.out.println("\n---------------------\n");
    }
}
