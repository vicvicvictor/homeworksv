package homeworkOnClass2;

public class Queue<T extends Comparable<T>>{
    private NodeQ<T> head;
    private NodeQ<T> tail;
    private int size;
    public Queue(){this.size=0;}
    public int size(){
        return size;
    }
    public boolean isEmpty(){
        return  size==0;
    }
    public T getHeadData(){
        return head.getData();
    }
    public T getTailData(){
        return tail.getData();
    }
    private boolean isElementOnQueue(T element){
        boolean isElementOnQueue= false;
        for (NodeQ<T> current = tail ; current!=null; current = current.getNext()){
            if (current.getData().equals(element)) {
                isElementOnQueue = true;
                break;
            }
        }
        return isElementOnQueue;
    }
    private boolean checkIfLegal(T element){
        /*
        NodeQ<T> newElement = new NodeQ(element);
        if (isElementOnQueue(element)){
            System.out.println("Element Repeted");
            size--;
            return true;
        }
        if (size ==0){
            tail = newElement;
            head = tail;
            return true;
        }
        return false;*/
        return true;
    }
    public void enqueue(T element){
        NodeQ<T> newElement = new NodeQ(element);
        if (isElementOnQueue(newElement.getData())){
            System.out.println("Element Repeted");
            return;
        }
        if (size ==0){
            tail = newElement;
            head = tail;
        }else {
            newElement.setNext(tail);
            tail = newElement;
        }
        size++;
    }
    private void reboot(){
        tail =null;
        head = null;
    }
    public T dequeue(){
        NodeQ<T> currentNode = tail;
        T elementRemoved;
        if (size ==1){
            reboot();
            elementRemoved = currentNode.getData();
        }else {
            while (currentNode.getNext()!=head){
                currentNode=currentNode.getNext();
            }
            elementRemoved=currentNode.getNext().getData();
            currentNode.setNext(null);
            head=currentNode;
        }
        size--;
        return elementRemoved;
    }

    public void display(){
        int i =0;
        for (NodeQ<T> currentNode = tail; currentNode!=null;currentNode = currentNode.getNext()){

            System.out.println(i + ".) "+currentNode.getData());
            i++;
        }
        System.out.println("tail: "+getTailData());
        System.out.println("head: "+getHeadData());
        System.out.println("\n---------------------\n");
    }
}
