package homeworkOnClass2;

public class NodeQ<T> {
    private T data;
    private NodeQ<T> next;
    public NodeQ(T data){
        this.data =data;
    }
    public T getData(){
        return data;
    }
    public NodeQ<T> getNext(){
        return next;
    }
    public void setNext(NodeQ<T> nodeQ){
        this.next = nodeQ;
    }
}
