package stack;

public class Main {
    public static void main(String[] args){
        Stack<Integer> stack= new Stack<>();
        stack.push(1);
        stack.display();
        System.out.println("-------");
        stack.push(2);
        stack.display();
        System.out.println("-------");
        stack.push(3);
        stack.display();
        System.out.println("-------");
        stack.push(4);
        stack.display();


        System.out.println(stack.toString());
        System.out.println("------------------------------");
        System.out.println("I am deleting the element: " + stack.pop());
        stack.display();
        System.out.println("------------");
        System.out.println("I am deleting the element: " + stack.pop());
        stack.display();
        System.out.println("------------");
        System.out.println("I am deleting the element: " + stack.pop());
        stack.display();
        System.out.println("------------");
        System.out.println("I am deleting the element: " + stack.pop());
        stack.display();
        System.out.println("------------");
    }
}
