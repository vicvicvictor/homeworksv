package stack;

    public class Stack<T extends Comparable<T>> {
    private Node<T> head;
    private int size;
    private Node<T> top;
    public Stack(){
        head = null;
        size =0;
        top = null;
    }
    @Override
    public String toString(){
        return "{Stack Top-> "+ top.getData()+"  "+ head.toString()+ "}";
    }
    public int size(){
        return size;
    }
    public boolean isEmpty(){
        return size==0;
    }
    public void push(T element){
        Node<T> newNode = new Node<>(element);
        if (isEmpty()){
            head = newNode;
            top =head;
        }else{
            top.setNext(newNode);
            top = newNode;
        }
        size++;
    }
    public T pop(){
        if (isEmpty()){return null;}
        T elementTop = top.getData();
        if (size()==1){
            head = null;
            top =head;
        }else {

            Node<T> currentNode = head;
            while (currentNode.getNext()!= top){
                currentNode = currentNode.getNext();
            }
            currentNode.setNext(null);
            top  =currentNode;
        }
        size--;
        return elementTop;
    }

    public T peek(){return top.getData();}

    public void display(){
        if (isEmpty()){
            System.out.println("The Stack is Empty, please push values");
        }else{
            for (Node<T> current  =head; current!=null; current = current.getNext()){
                System.out.println(current.getData());
            }
            System.out.println("Top : " +peek());
        }

    }

}
