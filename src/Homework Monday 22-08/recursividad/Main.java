package recursividad;

public class Main {
    public static void main(String[] args){
        // 1.- sacar screenshot de la salida del metodo
        // 2.- sacar screenshot, del callStack (Debug) con un breakpoint en LlamadasEncadenadas.doFour
        LlamadasEncadenadas.doOne();

        // descomentar la llamda a: Infinito.whileTrue()
        // 1.- sacar screenshot de la exception
        // 2.- a continuacion describir el problema:
        // Descripcion: como el metodo whileTrue se autollama a si mismo sin niguna condicional ni nada, lo que pasa es que
        //existen demasiadas llamadas recursivas y esto causa desbordamiento de la pila o tambien conocido como (STACK OVERFLOW).
        // 3.- volver a comentar la llmanda a: Infinito.whileTrue()
        //Infinito.whileTrue();

        // descomentar la llamda a: Contador.contarHasta(), pasnado un humer entero positivo
        // 1.- sacar screenshot de la salida del metodo
        // 2.- a continuacion describir como funciona este metodo:
        // Descripcion: Lo que este metodo hace es que disminuye el contador que recibe con recursion y con una condicion
        // que es que sea mayor a 0, de esta manera, la variable contador se disminuye el valor a uno y se impreme antes de que
        //se disminuya.
        //Contador.contarHasta(7);

        System.out.println(RecursividadvsIteracion.factorialIter(5));
        System.out.println(RecursividadvsIteracion.factorialRecu(5));

    }
}