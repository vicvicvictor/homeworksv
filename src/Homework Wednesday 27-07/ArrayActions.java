import java.security.KeyStore;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ArrayActions {
    public Map<Integer, Integer> countNumbers(int arr[]){
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < arr.length; i++) {
            if (!map.containsKey(arr[i])){
                map.put(arr[i], 1);
            }else{
                map.put(arr[i], map.get(arr[i])+1);
            }
        }
        return map;
    }
    public int[] mostFrequent(int arr[]){
        int[] finalResult = new int[2];
        Map<Integer, Integer> map = countNumbers(arr);
        Integer biggestKey = null;
        Integer biggestValue = null;

        for (Map.Entry<Integer,Integer> entry : map.entrySet()) {
            Integer key = entry.getKey();
            Integer value = entry.getValue();
            if(biggestValue == null || value > biggestValue){
                biggestValue = value;
                biggestKey = key;
            }

        }
        finalResult[0] = biggestKey;
        finalResult[1] = biggestValue;
        return finalResult;
    }
    public void printMostFrequent(int arr[]){
        System.out.print("(");
        for (int i = 0; i < arr.length; i++) {
                System.out.print(arr[i]+ " , ");
        }
        System.out.print(")");
    }
    public void printAll(int arr[]){
        Map<Integer,Integer> map = countNumbers(arr);
        List<Map.Entry<Integer, Integer>> lst = new ArrayList<>(map.entrySet());
        //list.forEach(System.out::println);
        lst.sort(Map.Entry.comparingByValue());
        int index =0;
        for (int i = lst.size() ;i >0 ; i--) {
            System.out.println((index+1)+".) "+ lst.get(i-1));
            index++;
        }
    }

}

