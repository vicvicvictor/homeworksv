package calculator;

public interface OperationsCalculable {
    void sum(int num);
    void multiply(int num);
    void power(int num);
    void factorial(int num);
}
