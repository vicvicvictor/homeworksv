package calculator.exceptions;

public class GiganticNumberException extends Exception {
    public GiganticNumberException(String message) {
        super(message);
    }
}