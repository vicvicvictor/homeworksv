package calculator.exceptions;

public class MissingParanthesisException extends Exception{
    public MissingParanthesisException(String message){
        super(message);
    }
}
