package calculator.exceptions;

public class GiganticOperationResultException extends Exception{
    public GiganticOperationResultException(String message){
        super(message);
    }
}
