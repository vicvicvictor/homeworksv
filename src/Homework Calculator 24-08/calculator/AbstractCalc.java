package calculator;

import stack.Stack;

public abstract class AbstractCalc {

    public abstract int calc(String string) throws Exception;
    public abstract void build(String string) throws Exception;
}
