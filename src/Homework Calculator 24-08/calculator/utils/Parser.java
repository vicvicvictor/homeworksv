package calculator.utils;

import stack.Stack;

public class Parser {
    public static Stack<Character> getTokens(String str) {
        Stack<Character> tokens = new Stack<>();
        for (int i = 0; i < str.length(); i++) {
            tokens.push(str.charAt(i));
        }
        return tokens;
    }
    public static int powerTo(int num, int pow){
        if (pow == 0){return 1;}
        else{return num * powerTo(num, pow - 1);}
    }
    public static int factorialTo(int factorial){
        int answer =factorial;
        while(factorial !=1 ){
            answer = answer * (factorial-1);
            factorial --;
        }
        return answer;
    }
    public static Stack<Character> invertStack(Stack<Character> stack){
        Stack<Character> newStackInverted = new Stack<>();
        Stack<Character> newStackInverted2 = new Stack<>();
        for (int i = 0, size = stack.size(); i < size; i++) {
            Character character = stack.pop();
            newStackInverted.push(character);
            newStackInverted2.push(character);
        }
        for (int i = 0, size = newStackInverted2.size(); i < size; i++) {
            Character character = newStackInverted2.pop();
            stack.push(character);
        }
        return newStackInverted;
    }
    public static Stack<Character> copyStack(Stack<Character> characterStack){
        return invertStack(invertStack(characterStack));
    }

}
