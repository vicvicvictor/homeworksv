package calculator;

import calculator.exceptions.*;
import calculator.utils.Parser;
import stack.Stack;

import javax.swing.plaf.IconUIResource;

import static calculator.utils.Parser.*;

public class Calculator extends AbstractCalc implements OperationsCalculable{
    private Stack<Character> stack;
    private Stack<Character> stackCopy;
    private Integer result;

    public Calculator(){
        this.stack = null;
        this.result = null;
    }

    @Override
    public void build(String string) throws Exception {
        stack = getTokens(string);
        stackCopy = copyStack(stack);
        verify();
    }
    private void verifyInvalidImcompleteOperationStructureOperation() throws InvalidImcompleteOperationStructureException {
        Stack<Character> stackCopyAux = copyStack(stackCopy);
        int counter =0;
        for (int i =0,size = stackCopy.size();i<size;i++){
            Character currentCharacter = stackCopy.pop();
                if (isCharacter(currentCharacter)){
                    counter++;
                    if (counter ==2){
                        throw new InvalidImcompleteOperationStructureException("Please write a correct Operation. Be careful");
                    }

                }else {
                    counter =0;
                }
        }

        if (!isItANumber(stackCopyAux.peek()) && stackCopyAux.peek() != ')' && stackCopyAux.peek() != '!' ){
            throw new InvalidImcompleteOperationStructureException("Please write a correct Operation. Be careful");
        }
    }
    private boolean isCharacter(Character character){
        return character == '+';
    }
    private void verifyInvalidOperationException(Character character) throws InvalidOperationException {
        if (getTypeOperation(character) ==0){
            throw new InvalidOperationException("The operation -> '" + character + "' is invalid. Be careful");
        }
    }
    private void verifyParenthesis() throws MissingParanthesisException {
        if (!operationWithParenthesis()){
            return;
        }
        int counter =0;
        for (int i =0,size = stackCopy.size();i<size;i++){
            Character currentCharacter = stackCopy.pop();
            if (i ==0 ||i == size-1){
                if (isCharacterAParenthesis(currentCharacter)){
                    counter++;
                }
            }
        }
        stackCopy =copyStack(stack);
        if (counter!=2){
            throw new MissingParanthesisException("The operation is missing parenthesis. Be careful.");
        }
    }

    private void verifyGiganticOperationResultException() throws GiganticOperationResultException {
        if (result >= (-2147483648) && result<0){
            throw new GiganticOperationResultException("The result is greater than 2147483647. Be careful");
        }
    }
    private void verifyGiganticNumberException(int num) throws GiganticNumberException{
        if (!(num > 0) && !(num <=2147483647)){
            throw new GiganticNumberException("The number "+ num +". You entered is greater than 2147483647. Be careful");
        }
    }

    private boolean verify() throws Exception {
        verifyParenthesis();
        return true;
    }
    private void verifyAfter() throws Exception{
        verifyGiganticOperationResultException();
    }

    private boolean isCharacterAParenthesis(Character character){
        return character == '(' || character==')';
    }
    private boolean operationWithParenthesis(){
        boolean answer = false;
        for (int i =0,size = stackCopy.size();i<size;i++){
            Character currentCharacter = stackCopy.pop();
            if (isCharacterAParenthesis(currentCharacter)){
                answer = true;
            }
        }
        stackCopy =copyStack(stack);
        return answer;
    }
    @Override
    public int calc(String string) throws Exception {
        build(string);
        Integer completeNumber =0, power =0, typeOperation=0;
        for(int i =0,size = stack.size(); i< size +1;i++){
            Character currentCharacter = stack.pop();
            if (isItANumber(currentCharacter)){
                completeNumber+= Integer.parseInt(currentCharacter+"")*powerTo(10, power);power++;
            }else{
                verifyGiganticNumberException(completeNumber);
                if (!stack.isEmpty()){typeOperation = getTypeOperation(currentCharacter);verifyInvalidOperationException(currentCharacter);}
                if (result == null){result = completeNumber;
                }else {operate(typeOperation, completeNumber);}
                power =0;completeNumber =0;
            }
        }
        verifyAfter();verifyInvalidImcompleteOperationStructureOperation();
        return result;
    }
    private boolean isIsItSpaceBlank(Character character){
        return character == ' ';
    }
    private int getTypeOperation(Character character){
        int answer = 0;
        switch (character){
            case '*':
                answer =1;
                break;
            case '+':
                answer =2;
                break;
            case '^':
                answer =3;
                break;
            case '!':
                answer =4;
                break;
            case ' ':
                answer =5;
                break;
            case ')':
                answer =6;
                break;
            case '(':
                answer =6;
                break;
        }
        return answer;
    }

    @Override
    public void sum(int completeNumber){
        result+= completeNumber;
    }
    @Override
    public void multiply(int completeNumber){
        result=result * completeNumber;
    }
    @Override
    public void power(int comleteNumber){
        result = powerTo( comleteNumber,result);
    }
    @Override
    public void factorial(int completeNumber){
        result += factorialTo(completeNumber);
    }
    private void operate(int type, int completeNumber){
         if (type ==1){multiply(completeNumber);
         }else if (type == 2){sum(completeNumber);
         }else if (type==3){power(completeNumber);
         }else if (type ==4){factorial(completeNumber);}
    }
    private boolean isItANumber(Character character){
        boolean answer = true;
        try{
            int x = Integer.parseInt(character+"");
        }catch (Exception e){
            answer = false;
        }
        return answer;
    }
    public Stack<Character> getStack(){
        return stack;
    }
    }




