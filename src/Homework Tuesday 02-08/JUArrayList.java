import java.util.*;

public class JUArrayList implements List<Integer> {
    private Integer[] array = new Integer[0];
    private int currentIndex =0;



    //Estos son los metodos obligatorios:
    @Override
    public int size() {
        return array.length;
    }

    @Override
    public boolean isEmpty() {
        return (array.length == 0);
    }

    @Override
    public Iterator<Integer> iterator(){
        return new LocalIterator(array);
    }


    /**
     * This is my LocaIterator class
     *
     * <p>
     *     This class is needs an array to be created.
     *     It itirates ove its array and changes all
     *     even numbers to 0.
     *
     * </p>
     */
    class LocalIterator implements Iterator<Integer>{
        private int currentSize;
        private int index;
        private Integer[] array;

        public LocalIterator(Integer[] array){
            this.array=array;
            currentSize=array.length;
            index=0;
        }
        @Override
        public boolean hasNext() {
            return currentSize>0;
        }
        @Override
        public Integer next() {
            Integer evenInteger;
            if(array[index]%2==0){
                evenInteger=0;
            }else {evenInteger=array[index];}
            index++;
            currentSize--;
            return evenInteger;
        }
    }



    @Override
    public boolean add(Integer integer) {
        if (currentIndex == size()){
            Integer[] arrayAux = new Integer[size()+1];
            for (int i = 0; i < size() ; i++) {
                arrayAux[i] = array[i];
            }
            array = arrayAux;
            array[currentIndex]= integer;
            currentIndex++;
        }
        return true;
    }

    @Override
    public boolean remove(Object object) {
        Integer[] arrAux = new Integer[size()-1];
        for (int i = 0, j =0; i < array.length; i++) {
            if (!array[i].equals(object)){
                arrAux[j] = array[i];
            }
            else {
                j--;
            }
            j++;
        }
        array = arrAux;
        return true;
    }


    @Override
    public void clear() {
        array = new Integer[0];
    }

    @Override
    public Integer get(int index) {
        return array[index];
    }

    @Override
    public Integer remove(int index) {
        Integer itemToRemove = index;
        Integer[] arrAux = new Integer[size()-1];
        for (int i = 0, j =0; i < array.length; i++) {
            if (!array[i].equals(array[index])){
                arrAux[j] = array[i];
            }
            else {
                j--;
            }
            j++;
        }
        array = arrAux;
        return itemToRemove;
    }






    //Estos son los metodos opcionales, para puntos extras:
    @Override
    public Integer set(int index, Integer element) {
        array[index]= element;
        return element;
    }

    @Override
    public void add(int index, Integer element) {
        Integer[] arrAux = new Integer[size()+1];
        for (int i = 0, j=0; i < arrAux.length; i++) {
            if (i ==index){
                arrAux[i] = element;
                continue;
            }
            arrAux[i] = array[j];
            j++;
        }
        array=arrAux;
    }
    @Override
    public boolean contains(Object object) {
        boolean isItThere= false;
        for (Integer integer : array) {
            if (integer.equals(object)) {
                isItThere = true;
                break;
            }
        }
        return isItThere;
    }

    @Override
    public Object[] toArray() {
        Object[] objectArray = new Object[size()];
        for (int i = 0; i < objectArray.length; i++) {
            objectArray[i]= array[i];
        }
        return objectArray;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        for (int i = 0; i < a.length; i++) {
            a[i]= (T) array[i];
        }
        return a;
    }


    public Integer[] getArray(){
        return array;
    }
    public void setArray(Integer[] newArray){
        this.array = newArray;
    }







    /*
        El trainer nos dijo que no implementemos estos metodos:
     */
    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<Integer> listIterator() {
        return null;
    }

    @Override
    public ListIterator<Integer> listIterator(int index) {
        return null;
    }

    @Override
    public List<Integer> subList(int fromIndex, int toIndex) {
        return null;
    }


    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends Integer> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends Integer> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }
}

