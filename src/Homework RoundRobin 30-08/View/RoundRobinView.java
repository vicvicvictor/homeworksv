package View;

import utils.Utils;

public class RoundRobinView {
    private Integer numTimes;
    public RoundRobinView(){this.numTimes = 150;}
    private void printLine(){
        System.out.println(Utils.BLUE+"-".repeat(Math.max(0, numTimes))+Utils.RESET);
    }
    public void viweTableHead(Integer quantum){
        System.out.println("\t\t\t\t\t\t\t\t\t\t\t\t\t\t"+Utils.BOLD+Utils.UNDERLINE+Utils.PURPLE+"ROUND ROBIN"+Utils.RESET);
        printLine();
        System.out.println(Utils.PURPLE+"Quantum: "+Utils.RESET +quantum);
        System.out.print(Utils.PURPLE+"Time Instance"+ Utils.RESET);
        System.out.print(Utils.BLUE+"\t|\t"+ Utils.RESET);
        System.out.print(Utils.PURPLE+"Process"+ Utils.RESET);
        System.out.print(Utils.BLUE+"\t\t|\t"+ Utils.RESET);
        System.out.print(Utils.PURPLE+"Arrival Time"+ Utils.RESET);
        System.out.print(Utils.BLUE+"\t\t|\t"+ Utils.RESET);
        System.out.print(Utils.PURPLE+"Initial Burst Time"+ Utils.RESET);
        System.out.print(Utils.BLUE+"\t|\t"+ Utils.RESET);
        System.out.print(Utils.PURPLE+"Remaining Burst Time"+ Utils.RESET);
        System.out.print(Utils.BLUE+"\t|\t"+ Utils.RESET);
        System.out.print(Utils.PURPLE+"Finished"+ Utils.RESET);
        System.out.print(Utils.BLUE+"\t|\t"+ Utils.RESET);
        System.out.print(Utils.PURPLE+"Completion Time\n"+ Utils.RESET);
        printLine();
    }
    public void viewTableBody(Integer lastInstanceTime, Integer timeInstance, Integer processId, Integer arrivalTime, Integer burstTime, Integer remainingBurstTime, boolean isFinished, Integer completionTime){
        String unknown = (completionTime == null)?Utils.YELLOW_BOLD+Utils.UNKNOWN:String.valueOf(completionTime);
        String finished = (!isFinished) ?Utils.RED+Utils.BAD :Utils.GREEN+Utils.GOOD;
        System.out.print(lastInstanceTime);
        System.out.print(Utils.RED+"-"+Utils.RESET);
        System.out.print(timeInstance);
        System.out.print(Utils.BLUE + "\t\t\t|\t" + Utils.RESET);
        System.out.print("P"+processId);
        System.out.print(Utils.BLUE + "\t\t\t|\t\t" + Utils.RESET);
        System.out.print(arrivalTime);
        System.out.print(Utils.BLUE + "\t\t\t|\t\t" + Utils.RESET);
        System.out.print(burstTime);
        System.out.print(Utils.BLUE + "\t\t\t\t|\t\t\t" + Utils.RESET);

        System.out.print(remainingBurstTime);
        System.out.print(Utils.BLUE + "\t\t\t\t|\t\t" + Utils.RESET);
        System.out.print(finished);
        System.out.print(Utils.BLUE + "\t\t|\t\t" + Utils.RESET);
        System.out.print(unknown+"\n");

        //System.out.println(lastInstanceTime+ "-" +timeInstance+"\t\t\t|\tP"+processId+"\t\t\t|\t\t"+arrivalTime+"\t\t\t|\t\t"+burstTime+"\t\t\t\t|\t\t"+remainingBurstTime+"\t\t\t\t\t|\t\t"+finished+"\t\t|\t\t"+unknown);
        printLine();
    }
}
