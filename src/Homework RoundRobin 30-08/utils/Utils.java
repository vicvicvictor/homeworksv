package utils;

public class Utils {
    public static final String RED = "\u001B[31m",
            GREEN = "\u001B[32m",
            BLUE = "\u001B[34m",
            PURPLE = "\u001B[35m",
            CYAN = "\u001B[36m",
            WHITE = "\u001B[37m",
            RESET="\u001B[0m",
            LIGHTBLUE = "\033[36m",
            YELLOW_BOLD = "\033[1;33m",
            GOOD = "✔",
            BAD = "✖",
            UNKNOWN ="❓",
            BOLD = "\u001b[1m",
            UNDERLINE = "\u001b[4m";
}
