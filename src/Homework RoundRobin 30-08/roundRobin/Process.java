package roundRobin;

public class Process {
    private Integer arrivalTime;
    private Integer completionTime;
    private Integer burstTime;
    private Integer remainingBurstTime;
    private boolean isFinished;
    private Integer id;
    private Integer startingTime;

    public Process( int id){
        this.id =id;
        this.startingTime =-1;
    }
    public Integer getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Integer arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public Integer getCompletionTime() {
        return completionTime;
    }

    public void setCompletionTime(Integer completionTime) {
        this.completionTime = completionTime;
    }

    public Integer getBurstTime() {
        return burstTime;
    }

    public void setRemainingBurstTime(Integer burstTime) {
        this.remainingBurstTime = burstTime;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public void setFinished(boolean finished) {
        isFinished = finished;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRemainingBurstTime() {
        return remainingBurstTime;
    }

    public void setBurstTime(Integer burstTime) {
        this.burstTime = burstTime;
    }

    public Integer getStartingTime() {
        return startingTime;
    }

    public void setStartingTime(Integer startingTime) {
        this.startingTime = startingTime;
    }
}
