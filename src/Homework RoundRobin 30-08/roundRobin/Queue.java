package roundRobin;


public class Queue {
    private Node head;
    private Node tail;
    private int size;
    public Queue(){this.size=0;}
    public int size(){
        return size;
    }
    public boolean isEmpty(){
        return  size==0;
    }
    public Process getHeadData(){
        return head.getData();
    }
    public Process getTailData(){
        return tail.getData();
    }
    public void enqueue(Process element){
        Node newElement = new Node(element);
        if (isEmpty()){
            tail = newElement;
            head = tail;
        }else {
            newElement.setNext(tail);
            tail = newElement;
        }
        size++;
    }
    private void reboot(){
        tail =null;
        head = null;
    }
    public Process dequeue(){
        Node currentNode = tail;
        Process elementRemoved;
        if (isEmpty()){
            System.out.println("This queque is empty");
            elementRemoved = null;
            size++;
        }else if (size ==1){
            reboot();
            elementRemoved = currentNode.getData();
        }else {
            while (currentNode.getNext()!=head){
                currentNode=currentNode.getNext();
            }
            elementRemoved=currentNode.getNext().getData();
            currentNode.setNext(null);
            head=currentNode;
        }
        size--;
        return elementRemoved;
    }

    public void display(){
        int i =1;
        if (isEmpty()){
            System.out.println("This queque is empty");
            return;
        }
        for (Node currentNode = tail; currentNode!=null;currentNode = currentNode.getNext()){
            Process currentProcess = currentNode.getData();
            System.out.println(i + ".) Process "+currentProcess.getId() + " Burst Time: "+ currentProcess.getBurstTime()+", ArrivalTime: "+currentProcess.getArrivalTime());
            i++;
        }
        System.out.println("\nTail: Process "+getTailData().getId());
        System.out.println("Head: Process "+getHeadData().getId());
        System.out.println("\n---------------------\n");
    }
}
