package roundRobin;

public class ProcessManager {
    private Queue readyQueue;
    private Integer maxBurstTime;
    private Integer time;
    ProcessManager(Queue readyQueue){
        this.readyQueue =readyQueue;
        this.time=0;
        this.maxBurstTime = 100;
    }
    ProcessManager(Queue readyQueue, int maxBurstTime){
        this.readyQueue =readyQueue;
        this.time=0;
        this.maxBurstTime = maxBurstTime;
    }
    ProcessManager(Queue readyQueue, int maxBurstTime, int numQueues){
        this.readyQueue =readyQueue;
        this.time=0;
        this.maxBurstTime = maxBurstTime;
        addProcesses(numQueues);
    }
    public void addProcesses(int num){
        for (int i = 1; i <= num; i++) {
            Process newProcess = new Process(i);
            setProcessRemainingBurstTime(newProcess);
            setProcessArrivalTime(newProcess);
            readyQueue.enqueue(newProcess);
            time++;
        }
    }
    private void setProcessRemainingBurstTime(Process process){
        int randomNum = (int)Math.floor(Math.random()*(maxBurstTime)+1);
        process.setRemainingBurstTime(randomNum);
        process.setBurstTime(randomNum);
    }
    private void setProcessArrivalTime(Process process){
        process.setArrivalTime(time);
    }
    public Queue getReadyQueue() {
        return readyQueue;
    }

    public void setReadyQueue(Queue readyQueue) {
        this.readyQueue = readyQueue;
    }

    public Integer getMaxBurstTime() {
        return maxBurstTime;
    }

    public void setMaxBurstTime(Integer maxBurstTime) {
        this.maxBurstTime = maxBurstTime;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }
}
