package roundRobin;

import View.RoundRobinView;

import static java.lang.Thread.sleep;

public class RoundRobin {
    private Queue finishedQueue;
    private Queue readyQueue;
    private final Integer quantum;
    private  Integer timeInstance;
    private RoundRobinView robinView;
    private Integer readyQueueSize;
    private Integer lastInstanceTime;

    public RoundRobin(Queue readyQueue, int... quantum){
        this.readyQueue = readyQueue;
        this.quantum =quantum.length>0?quantum[0]:20;
        this.timeInstance= 0;
        this.robinView = new RoundRobinView();
        this.finishedQueue = new Queue();
        this.readyQueueSize = readyQueue.size();
        this.lastInstanceTime =0;
    }
    public void start() throws InterruptedException {
        robinView.viweTableHead(quantum);
        for (int i =0; i<readyQueueSize;i++){
            Process currentProcess = readyQueue.dequeue();
            roundRobinAlgorithm(currentProcess);
            sleep(500);
            robinView.viewTableBody(lastInstanceTime,timeInstance, currentProcess.getId(), currentProcess.getArrivalTime(), currentProcess.getBurstTime(), currentProcess.getRemainingBurstTime(), currentProcess.isFinished(),currentProcess.getCompletionTime());
            lastInstanceTime = timeInstance;
        }
    }
    private void setStartingTimeProcess(Process process){
        if (process.getStartingTime()==-1){
            process.setStartingTime(lastInstanceTime);
        }
    }
    public void roundRobinAlgorithm(Process process){
        Integer remainingBurst= process.getRemainingBurstTime();
        setStartingTimeProcess(process);
        if (remainingBurst > quantum){
            process.setRemainingBurstTime(remainingBurst-quantum);
            timeInstance+=quantum;
            readyQueue.enqueue(process);
            readyQueueSize++;
        }else {
            timeInstance+=remainingBurst;
            process.setRemainingBurstTime(0);
            process.setFinished(true);
            process.setCompletionTime(timeInstance- process.getStartingTime());
            finishedQueue.enqueue(process);
        }
    }

    public Queue getReadyQueue() {
        return readyQueue;
    }

    public void setReadyQueue(Queue readyQueue) {
        this.readyQueue = readyQueue;
    }

    public Integer getQuantum() {
        return quantum;
    }

    public RoundRobinView getRobinView() {
        return robinView;
    }

    public void setRobinView(RoundRobinView robinView) {
        this.robinView = robinView;
    }

    public Queue getFinishedQueue() {
        return finishedQueue;
    }

    public void setFinishedQueue(Queue finishedQueue) {
        this.finishedQueue = finishedQueue;
    }
}
