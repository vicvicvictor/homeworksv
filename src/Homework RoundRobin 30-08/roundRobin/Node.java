package roundRobin;


public class Node {
    private Process data;
    private Node next;
    public Node(Process data){
        this.data =data;
    }
    public Process getData(){
        return data;
    }
    public Node getNext(){
        return next;
    }
    public void setNext(Node node){
        this.next = node;
    }
}
