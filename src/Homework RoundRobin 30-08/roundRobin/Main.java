package roundRobin;

import View.RoundRobinView;
import utils.Utils;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Queue queue = new Queue();
        //ProcessManager processManager = new ProcessManager(queue);
        //processManager.addProcesses(10);

/*
        Process p1 = new Process( 1);
        Process p2 = new Process( 2);
        Process p3 = new Process( 3);
        Process p4 = new Process( 4);
        p1.setRemainingBurstTime(53);
        p2.setRemainingBurstTime(17);
        p3.setRemainingBurstTime(68);
        p4.setRemainingBurstTime(24);
        p1.setBurstTime(53);
        p2.setBurstTime(17);
        p3.setBurstTime(68);
        p4.setBurstTime(24);
        queue.enqueue(p1);
        queue.enqueue(p2);
        queue.enqueue(p3);
        queue.enqueue(p4);
        */

        ProcessManager processManager = new ProcessManager(queue,100, 5);
        RoundRobin roundRobin = new RoundRobin(queue,20);
        Queue finishedQueue = roundRobin.getFinishedQueue();
        roundRobin.start();
        //queue.display();
        //finishedQueue.display();

    }
}
