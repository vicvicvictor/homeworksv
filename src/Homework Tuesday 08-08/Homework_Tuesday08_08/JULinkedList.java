package Homework_Tuesday08_08;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class JULinkedList<T>  implements List<T>{
    private Node<T> head;
    private Node<T> actualNode;
    private int size;
    public JULinkedList(){
        size =0;
    }
    public void print(){
        System.out.println(head);
    }
    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size==0;
    }

    @Override
    public boolean contains(Object o) {
        Node<T> currentNode = head;
        boolean contains = false;
        while (currentNode != null){
            if (currentNode.getData().equals(o)) {
                contains = true;
                break;
            }
            currentNode = currentNode.getNext();
        }
        return contains;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<>() {
            int c = 0;
            Node<T> current = head;

            public boolean hasNext() {
                return size > c;
            }
            public T next() {
                if (c > 0) {
                    current = current.getNext();
                }
                c++;
                return current.getData();
            }
        };
    }
    @Override
    public boolean add(T t) {
        Node<T> newNode = new Node<>(t);
        if (size==0){
            head = newNode;
            actualNode=head;
        }else {
            actualNode.setNext(newNode);
            actualNode = newNode;
        }
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        for (Node<T> currentNode = head, previousNode = head; currentNode!=null;
             previousNode =currentNode, currentNode= currentNode.getNext() ){
            if (currentNode.getData().equals(o)){
                if (currentNode.equals(head)){
                    head = head.getNext();
                } else {previousNode.setNext(currentNode.getNext());}
            }
        }
        size--;
        return false;
    }

    @Override
    public void clear() {
        head = new Node<T>();
        size =0;
    }

    @Override
    public T get(int index) {
        T dataToReturn = null;
        int currentIndex=0;
        for (Node<T> currentNode = head; currentNode!= null; currentNode= currentNode.getNext(),currentIndex++) {
            if (index==currentIndex){
                dataToReturn = currentNode.getData();
            }
        }
        return dataToReturn;
    }

    @Override
    public T set(int index, T element) {
        int currentIndex =0;
        Node<T> newNode = new Node<>(element);

        for (Node<T> currentNode = head, previousNode =head ; currentNode!= null;
             previousNode = currentNode, currentNode= currentNode.getNext(), currentIndex++){
            if (index==currentIndex){
                if (currentNode.equals(head)){
                    newNode.setNext(head);
                    head = newNode;
                }else{
                    previousNode.setNext(newNode);
                    newNode.setNext(currentNode);
                }
                break;
            }
        }
        size++;
        return element;
    }

    @Override
    public T remove(int index) {
        int currentIndex =0;
        for (Node<T> currentNode = head, previousNode = head ; currentNode!=null;
             previousNode =currentNode, currentNode = currentNode.getNext()) {
            if (currentIndex == index){
                if (currentIndex==0){
                    head = head.getNext();
                }else{previousNode.setNext(currentNode.getNext());}

            }
            currentIndex++;
        }
        size--;
        return null;
    }

    @Override
    public int indexOf(Object o) {
        Node<T> currentNode = head;
        int currentIndex=0;
        while(currentNode!=null){
            if (currentNode.getData().equals(o))break;
            currentIndex++;
            currentNode = currentNode.getNext();
        }
        return currentIndex;
    }

    @Override
    public int lastIndexOf(Object o) {
        Node<T> currentNode = head;
        int currentIndex=0;
        int objectIndex =0;
        while(currentNode!=null){
            if (currentNode.getData().equals(o)){
                objectIndex=currentIndex;
            }
            currentIndex++;
            currentNode = currentNode.getNext();
        }
        return objectIndex;
    }

    @Override
    public void add(int index, T element) {

    }
    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public Object[] toArray() {
        Object[] arr = new Object[size];
        int i =0;
        for (Node<T> currentNode = head ; i!=size;currentNode=currentNode.getNext()) {
//            print();
            arr[i] = currentNode.getData();
            i++;
        }
        return arr;
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (T itarator: c){
            add(itarator);
        }
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }


    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }


}
