package Homework_Monday18_08;

public interface List<T> extends Comparable<T> {
    /*
        I AM VICTOR VILLCA AND I AM IN GROUP F, SO I HAD TO IMPLEMENT THIS 5 METHODS:
    */
    int size();

    boolean isEmpty();

    boolean add(T data);

    boolean remove(T data);

    T get(int index);

    // intercambia un elemento en el índice x con otro en el índice y
    void xchange(int x, int y);

}

