package Homework_Monday18_08;

public class DoubleCircularLinkedList<T> implements List<T>{
    private Node<T> head;
    private Node<T> tail;
    private int size;
    DoubleCircularLinkedList(){
        this.head = null;
        this.tail = null;
        this.size =0;
    }
    public String toString()
    {
        int i=0;
        StringBuilder lstString = new StringBuilder();
        for (Node<T> x = head; i<size; x = x.getNext()) {
            lstString.append("(").append(x.getData()).append(") ");
            i++;
        }
        return lstString.toString();
    }
    public void v(){
        for (Node<T> c =head; c!=null; c =c .getNext()){
            System.out.println(c.getData());
        }
    }
    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size==0;
    }

    @Override
    public boolean add(T data) {
        if (size ==0){
            Node<T> newNode = new Node<>(data);
            head = tail = newNode;
            newNode.setNext(head);
            newNode.setPrevious(tail);
        }else{
            tail = head.getPrevious();
            Node<T> newNode = new Node<>(data);
            newNode.setNext(head);
            head.setPrevious(newNode);
            newNode.setPrevious(tail);
            tail.setNext(newNode);
        }
        size++;
        return true;
    }

    @Override
    public boolean remove(T data) {
        if (isEmpty()){return false;}
        Node<T> currentNode = head;
        Node<T> previous = null;
        while (currentNode.getData() != data){
            if (currentNode.getNext().equals(head)){
                System.out.println("No value found");
                return false;
            }
            previous = currentNode;
            currentNode=currentNode.getNext();
        }
        if (currentNode.getNext().equals(head)&&previous.equals(null)){
            return false;
        }
        if (currentNode.equals(head)){
            previous = head.getPrevious();
            head = head.getNext();
            previous.setNext(head);
            head.setPrevious(previous);
        }else if(currentNode.getNext().equals(head)){
            previous.setNext(head);
            head.setPrevious(previous);

        }else {
            Node<T> aux = currentNode.getNext();
            previous.setNext(aux);
            aux.setPrevious(previous);
        }
        size--;
        return true;
    }

    @Override
    public T get(int index) {
        Node<T> currentNode = head;
        int localIndex =0;
        while(index != localIndex){
            currentNode = currentNode.getNext();
            localIndex++;
        }
        return currentNode.getData();
    }

    @Override
    public void xchange(int x, int y) {
        Node<T> currentNode = head;
        int localIndex =0;
        while(localIndex != x){
            currentNode =currentNode.getNext();
            localIndex++;
        }
        T valueAux = currentNode.getData();
        remove(currentNode.getData());
        addSpecificPlace(get(y), localIndex);
        currentNode = head;
        localIndex=0;
        while(localIndex!=y){
            currentNode = currentNode.getNext();
            localIndex++;
        }
        remove(currentNode.getData());
        addSpecificPlace(valueAux, localIndex);
    }
    private void addSpecificPlace(T element, int index){
        index++;
        Node<T> aux, newNode ;
        newNode = new Node<>(element);
        aux =head;
        for (int j = 1; j < index-1; j++) {
            aux =aux.getNext();
        }
        newNode.setNext(aux.getNext());
        aux.getNext().setPrevious(newNode);
        aux.setNext(newNode);
        newNode.setPrevious(aux);
        size++;
    }
    @Override
    public int compareTo(T o) {
        return 0;
    }
}
