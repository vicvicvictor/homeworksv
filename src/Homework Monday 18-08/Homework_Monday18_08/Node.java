package Homework_Monday18_08;

public class Node<T> {
    private Node<T> next;
    private Node<T> previous;
    private T data;
    Node(T data){
        this.data=data;
    }
    public Node<T> getNext() {
        return next;
    }

    public void setNext(Node<T> next) {
        this.next = next;
    }

    public Node<T> getPrevious() {
        return previous;
    }

    public void setPrevious(Node<T> previous) {
        this.previous = previous;
    }
    public T getData() {
        return data;
    }
    @Override
    public String toString(){
        return "data =" + this.data;
    }
}
