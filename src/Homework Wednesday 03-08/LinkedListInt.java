public class LinkedListInt {

    private Node root;
    private Node lastNode;
    private int size;
    public LinkedListInt(){}
    public LinkedListInt(int... dataArr){
        addAll(dataArr);
    }
    private void addAll(int... dataArr){
        for (int data : dataArr){
            add(data);
        }
    }
    public void add(int data){
        Node newNode = new Node(data);
        if (lastNode == null){
            root = newNode;
        }else {
            lastNode.setNext(newNode);
        }
        lastNode = newNode;

        size++;
    }

    public int get(int index){

        int foundData=-1;
        if (index>=0 && index <size){
            Node nodeAux = root;
            for (int i = 0; i != index && i<size; i++) {
                nodeAux = nodeAux.getNext();
            }
            foundData = nodeAux.getData();
        }
        return foundData;
    }

    public void remove(int data){
        for(Node node = root, previusNode=root; node != null; previusNode =node, node=node.getNext()){
            if (node.getData() == data){
                if (node == root){
                    root=root.getNext();
                }
                previusNode.setNext(     node.getNext()    );
            }
        }
        size--;
    }

    public String toString(){
        StringBuilder lst= new StringBuilder();
        lst.append("[ ");
        for(Node node = root; node != null;node=node.getNext()){
            lst.append(node.getData()).append(" ");
        }
        lst.append("]\n");
        return lst.toString();
    }
    public boolean isEmpty(){
        return size ==0;
    }
    public int size(){
        return size;
    }
    public void printLinkedList(){
        System.out.print("[ ");
        for(Node node = root; node != null;node=node.getNext()){
            System.out.print(node.getData() + " ");
        }
        System.out.println("]\n");
    }


}
