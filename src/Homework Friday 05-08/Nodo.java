public class Nodo {
    int data;
    Nodo next;

    public Nodo() {
    }

    public Nodo(int n) {
        this.data = n;
    }

    @Override
    public String toString() {
        return " [ Node{" +
                "data=" + data +
                ", next=" + next +
                "}] ";
    }

    public static int getNth(Nodo nodo, int index) throws Exception {
        if (index < 0) {
            throw new IllegalArgumentException();
        }
        for (int i = 0; i < index; i++) {
            nodo = nodo.next;
        }
        return nodo.data;
    }

    public static Nodo append(Nodo listA, Nodo listB) {
        if (listA == null) {
            return listB;
        }
        Nodo currentLst = listA;

        while (currentLst.next != null) {
            currentLst = currentLst.next;
        }
        currentLst.next = listB;
        return listA;

    }
}

