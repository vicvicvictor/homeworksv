import java.util.ArrayList;

public class Canvas {
    private int height, width;
    private Character currentCharacter;
    private ArrayList<ArrayList<Character>> canvas = new ArrayList<ArrayList<Character> >();
    private String canvasString = "";
    public Canvas(int height, int widht){
        this.height = height;
        this.width = widht;
        createCanvas(widht, height);
    }
    private void fillCanvasWithEmptyArrays(){
        for (int i = 0; i < height; i++) {
            canvas.add(new ArrayList<Character>());
        }
    }
    private void fillAllItemsWithCharacter(Character character){
        for (int i = 0; i < canvas.size(); i++) {
            for (int j = 0; j < width; j++) {
                canvas.get(i).add(character);
            }
        }
    }
    public ArrayList<ArrayList<Character>> createCanvas(int width, int height){
        fillCanvasWithEmptyArrays();
        fillAllItemsWithCharacter(' ');
        return canvas;
        }

    public String drawCanvas(){
        int width= canvas.get(0).size();
        String horizontalLine = multiplyCharacter('-',width+2);
        System.out.println(horizontalLine);
        canvasString+=horizontalLine+"\n";
        for (int i = 0; i < canvas.size(); i++) {
            printItemsOnAList(i);
            canvasString+="\n";
            System.out.print("\n");
        }
        canvasString+=horizontalLine;
        System.out.println(horizontalLine);
        System.out.println( canvasString);
        return canvasString;
    }
    private void printItemsOnAList(int listIndex) {
        for (int j = 0; j < canvas.get(listIndex).size(); j++) {
            currentCharacter = canvas.get(listIndex).get(j);
            if (j == 0) {
                canvasString+="|"+currentCharacter;
                System.out.print("|" + currentCharacter);
            } else if (j == width - 1) {
                canvasString+=currentCharacter+"|";
                System.out.print(currentCharacter + "|");
            } else {
                canvasString+=currentCharacter;
                System.out.print(currentCharacter);
            }
        }
    }
    private String multiplyCharacter( Character character, int n_times){
        String characters = "";
        for (int i = 0; i < n_times; i++) {
            characters+= character;
        }
        return characters;
    }

    public Canvas draw(int x1, int y1, int x2, int y2){
        int distanceY=Math.abs(x1-x2)+1;
        int distanceX=Math.abs(y1-y2)+1;
        if (y1==y2){
            if(x1<x2){
                drawNormalLineOnX(x1,y1,1,distanceY);
            }else{
                drawNormalLineOnX(x1,y1,-1, distanceY);
            }
        }else{
            if(y1<y2){
                drawNormalLineOnY(x1,y1,1,distanceX);
            }else{
                drawNormalLineOnY(x1,y1,-1,distanceX);
            }
        }
        return this;
    }

    private void drawNormalLineOnX(int x,int y,int sign, int distance){
        for (int i = 0;  i< distance; i++) {
            canvas.get(x).set(y,'x');
            x=x+(sign);
        }
    }
    private void drawNormalLineOnY(int x,int y,int sign, int distance){
        for (int i = 0;  i< distance; i++) {
            canvas.get(x).set(y,'x');
            y=y+(sign);
        }
    }
}
