import java.util.ArrayList;

public class Main {

    public static void main(String args[]) {
        ArrayList<Color> colorList = new ArrayList<>();
        colorList.add(Color.BLUE);
        colorList.add(Color.BLACK);
        colorList.add(Color.GREEN);
        colorList.add(Color.RED);
        colorList.add(Color.BLACK);
        colorList.add(Color.BLACK);
        colorList.add(Color.GREEN);
        colorList.add(Color.RED);
        colorList.add(Color.GREEN);
        colorList.add(Color.RED);
        ColorActions colorActions = new ColorActions();

        int numberOfDistinctColors =colorActions.numberOfDistinctColors(colorList);
        int blueCount = colorActions.colorOccurrence(colorList, Color.BLUE);
        int blackCount = colorActions.colorOccurrence(colorList, Color.BLACK);

        System.out.println("Number of Distinct colors: " + numberOfDistinctColors);
        System.out.println("Blue color count: " + blueCount);
        System.out.println("Black color count: " + blackCount);
    }

}
