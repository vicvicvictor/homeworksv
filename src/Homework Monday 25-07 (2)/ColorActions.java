import java.util.*;

public class ColorActions implements MysteryColorAnalyzer{
    @Override
    public int numberOfDistinctColors(List<Color> mysteryColors) {
        ArrayList<String> colors = new ArrayList<>();
        for (Color color:
             mysteryColors) {
            if (! isStringInList( colors, color.name()) ){
                colors.add(color.name());
            }
        }
        return colors.size();
    }

    @Override
    public int colorOccurrence(List<Color> mysteryColors, Color color) {
        int counter = 0;
        for (Color c:
             mysteryColors) {
            if (c.name().equals(color.name())){
                counter++;
            }
        }
        return counter;
    }
    private boolean isStringInList(ArrayList<String> stringList, String string){
        boolean value = false;
        for (String x:
             stringList) {
            if(string.equals(x)){
                value = true;
            }
        }
        return value;
    }
}
