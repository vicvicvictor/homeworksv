public class Arrs {
//Esta es la segunda solucion que intente pero solo funciona con los numeros impares
    public boolean done(int[] arr){
        boolean isDone= false;
        int counter=0;
        for (int i = 0, k=1; i < arr.length; i++) {
            if (isOdd(arr[i]) && isEven(k)){
                counter++;
            }
            k++;
        }
        return true;
    }
    public int odd(int[] arrBase){
        int[] toLoop;
        toLoop = arrBase;
        Integer beforeNum= null;
        int indexCounter =0;
        int newItem =0;
        for (int num:arrBase) {
            if(beforeNum==null){beforeNum=num;}
            if (isOdd(beforeNum, num)){
                newItem+=beforeNum+num;
            }else{
                    arrBase = createNewModifiedArray(arrBase, indexCounter, newItem);
                    indexCounter =0;
                    newItem=0;
                }
            indexCounter++;
            beforeNum = num;
            }
        return arrBase.length;
        }

    public int[] even(int[] arrBase){
        int[] toLoop;
        toLoop = arrBase;
        Integer beforeNum= null;
        int indexCounter =0;
        int newItem =0;
        for (int num:arrBase) {
            if(beforeNum==null){beforeNum=num;}
            if (isEven(beforeNum, num)){
                newItem+=beforeNum+num;
            }else{
                arrBase = createNewModifiedArray(arrBase, indexCounter, newItem);
                indexCounter =0;
                newItem=0;
            }
            indexCounter++;
            beforeNum = num;
        }
        return arrBase;
    }


    private boolean isOdd(int x){
        return (x%2==1 );
    }
    private boolean isEven(int x){
        return (x%2==0 );
    }
    private boolean isOdd(int x, int y){
        return (x%2==1 && y%2==1);
    }
    private boolean isEven(int x, int y){
        return (x%2==0 && y%2==0);
    }

    public int[] range(int x){
        int[] arr = new int[x+1];
        for (int i = 0, k =0; i < arr.length; i++) {
            arr[k++]=i;
        }
        return arr;
    }
    public int[] createNewModifiedArray(int[] arrBase, int indexCounter, int newItem){
        int size = arrBase.length-indexCounter;
        int[] arr = new int[size];
        arr[0]=newItem;
        for (int i = 0, k=1; i < arr.length+indexCounter; i++) {

            if (!isOnList(i, range(indexCounter))){
                arr[k++]= arrBase[i];
            }
        }
        return arr;
    }
    private boolean isOnList(int i,int[] arr){
        boolean isIt = false;
        for (int num:
             arr) {
            if (i==  num){
                isIt= true;
            }
        }
        return isIt;
    }

}


