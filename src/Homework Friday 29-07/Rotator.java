public class Rotator {

    /**
     * This method rotates an Array
     *
     * <p>It rotates n times according
     * to the given parameter. It rotates to
     * the right if the n parameter is positive
     * and to the left if it is negative. If n is
     * 0, it does not rotate at all</p>
     * @param objects the list to rotate
     * @param n number of times for rotation
     * @return a rotated array
     */
    public Object[] rotate(Object[] objects, int n) {
        while (n > 0){
            objects= rotePositive(objects);
            n--;
        }
        while (n < 0){
            objects = roteNegative(objects);
            n++;
        }
        return objects;
    }

    /**It rotates an Array to the right
     *
     * @param objects the array to rotate
     * @return a rotated array
     */
    public Object[] rotePositive(Object[] objects){
        Object[] rotatedObj = new Object[objects.length];
        for (int i = 0; i < objects.length; i++) {
            System.out.println(objects[i]);
            if (i == objects.length-1){
                rotatedObj[0]= objects[i];
                break;
            }
            rotatedObj[i+1] = objects[i];
        }
        return rotatedObj;
    }

    /**It rotates an Array to the left
     *
     * @param objects the array to rotate
     * @return a rotated array
     */
    public Object[] roteNegative(Object[] objects){
        Object[] rotatedObj = new Object[objects.length];
        for (int i = 0; i < objects.length; i++) {
            System.out.println(objects[i]);
            if (i == objects.length-1){
                rotatedObj[i]= objects[0];
                break;
            }
            rotatedObj[i] = objects[i+1];
        }
        return rotatedObj;
    }
}
