package HomeworkFriday_12_08;
public class LinkedBag<T extends Comparable<T> > implements Bag<T> {
    private int size;
    private Node<T> root;
    public boolean add(T data) {
        Node<T> node = new Node<>(data);
        node.setNext(root);
        root = node;
        size++;

        return true;
    }
    public String toString() {
        return "(" + size + ")" + "root=" + root;
    }
    public void selectionSort() {
        for (Node<T> current= root; current!=null;current= current.getNext() ){
            Node<T> minimun = current;
            for (Node<T> current2 = current; current2!=null; current2=current2.getNext()) {
                if ((Integer) current2.getData()< (Integer) minimun.getData()){
                    minimun = current2;
                }
            }
            T aux = current.getData();;
            current.setData(minimun.getData());
            minimun.setData(aux);
        }
    }
    private T getNode(int index) {
        T dataToReturn = null;
        int currentIndex=0;
        for (Node<T> currentNode = root; currentNode!= null; currentNode= currentNode.getNext(),currentIndex++) {
            if (index==currentIndex){
                dataToReturn = currentNode.getData();
            }
        }
        return dataToReturn;
    }
    public void bubbleSort() {
        boolean continueSorting = true;
        while (continueSorting){
            continueSorting = false;
            Node<T> currentNode = root, previousNode = root, nextNode =currentNode.getNext();
            while (nextNode!=null){
                if ((Integer)currentNode.getData()>(Integer) nextNode.getData()){
                    continueSorting=true;
                    if (currentNode.equals(root)){
                        T aux = root.getData();
                        root.setData(nextNode.getData());
                        nextNode.setData(aux);

                    }else{
                        T aux2 = currentNode.getData();
                        currentNode.setData(nextNode.getData());
                        nextNode.setData(aux2);
                    }
                }
                currentNode = currentNode.getNext();

                nextNode =currentNode.getNext();
            }
        }

    }

    @Override
    public void xchange(int y, int x) {

    }

}
