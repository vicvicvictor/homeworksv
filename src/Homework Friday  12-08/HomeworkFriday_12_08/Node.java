package HomeworkFriday_12_08;

class Node<T> {
    private T data;
    private Node<T> next;
    public Node(T data) { this.data = data; }
    public T getData() { return data;}
    public Node<T> getNext() { return next; }
    public void setNext(Node<T> node) { next = node; }
    public void setData(T data){
        this.data = data;
    }
    public String toString() {
        return "{data=" + data + ", sig->" + next + "}";
    }
}